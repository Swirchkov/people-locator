/**
 * Created by Vladimir on 29.10.2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var main_component_1 = require('./main.component');
var login_component_1 = require("./login.component");
var register_component_1 = require("./register.component");
var schedule_component_1 = require("./schedule.component");
var create_event_component_1 = require("./create.event.component");
var update_event_component_1 = require("./update.event.component");
var delete_event_component_1 = require("./delete.event.component");
var search_user_component_1 = require("./search.user.component");
var routes = [
    {
        path: '',
        redirectTo: '/main',
        pathMatch: 'full'
    },
    {
        path: 'main',
        component: main_component_1.MainComponent
    },
    {
        path: "login",
        component: login_component_1.LoginComponent
    },
    {
        path: 'register',
        component: register_component_1.RegisterComponent
    },
    {
        path: "schedule",
        component: schedule_component_1.ScheduleComponent
    },
    {
        path: "createEvent",
        component: create_event_component_1.CreateEventComponent
    },
    {
        path: 'update-event',
        component: update_event_component_1.UpdateEventComponent
    },
    {
        path: 'delete-event',
        component: delete_event_component_1.DeleteEventComponent
    },
    {
        path: 'userSearch',
        component: search_user_component_1.SearchUserComponent
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map