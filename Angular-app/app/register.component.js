/**
 * Created by Vladimir on 30.10.2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var User_1 = require("./Models/User");
var account_service_1 = require("./Services/account.service");
var Session_1 = require("./Library/Session");
var RegisterComponent = (function () {
    function RegisterComponent(accountService, router) {
        this.accountService = accountService;
        this.router = router;
        this.hasErrors = false;
        this.user = new User_1.User();
    }
    RegisterComponent.prototype.validateImage = function () {
        // check image file from input 
        var inputElement = document.getElementsByName('Image')[0];
        var file = inputElement.files[0];
        if (!file.name.endsWith('jpg') && !file.name.endsWith('jpeg') && !file.name.endsWith('png')) {
            inputElement.setCustomValidity("Invalid file format");
            console.log('invalid');
        }
        else {
            console.log('valid');
            //reset custom validity
            inputElement.setCustomValidity("");
        }
        console.log(inputElement.validity);
    };
    RegisterComponent.prototype.validateForm = function () {
        var form = document.getElementsByName('registerForm')[0];
        var validity = form.checkValidity();
        if (!validity) {
            return validity;
        }
        return validity;
    };
    RegisterComponent.prototype.getImageFile = function () {
        var inputElement = document.getElementsByName('Image')[0];
        return inputElement.files[0];
    };
    RegisterComponent.prototype.Register = function () {
        var _this = this;
        window.event.preventDefault();
        if (!this.validateForm()) {
            return false;
        }
        this.accountService.RegisterUser(this.user, this.getImageFile()).then(function (res) {
            if (res.IsSuccess) {
                Session_1.Session.AuthenticatedUser = _this.user;
                Session_1.Session.SaveToken(res.Token);
                _this.router.navigate(['/schedule']);
            }
            else {
                _this.hasErrors = true;
            }
        });
        return false;
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: "register",
            templateUrl: "app/Views/register.component.html",
            styleUrls: ['app/Styles/account.css']
        }), 
        __metadata('design:paramtypes', [account_service_1.AccountService, router_1.Router])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map