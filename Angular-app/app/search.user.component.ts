import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

//services
import { AccountService } from "./Services/account.service";
import { FriendService } from "./Services/friend.service";
import { RequestService } from "./Services/request.service";

// library
import { Session } from "./Library/Session";

//models
import { User } from "./Models/User";
import { SearchedUser, UserRelation } from "./Models/SearchedUser";
import { Friend } from "./Models/Friend";
import { FriendUser } from "./Models/FriendUser";
import { MeetingRequest } from "./Models/MeetingRequest";

@Component({
    selector: 'searchUser',
    templateUrl: '/app/Views/search.user.component.html',
    styleUrls: [ 'app/Styles/search.user.component.css' ]
})
export class SearchUserComponent implements OnInit {

    private AuthenticatedUser: User;

    // friends cud part
    private addingFriend: SearchedUser = null;
    private answeringFriend: SearchedUser = null;
    private deletingFriend: SearchedUser = null; 

    // relation statuses
    private NO_RELATION: UserRelation = UserRelation.NoRelation;
    private SENT_REQUEST: UserRelation = UserRelation.SentRequest;    
    private RECEIVED_REQUEST: UserRelation = UserRelation.ReceivedRequest;
    private FRIENDS: UserRelation = UserRelation.Friends;

    // page statuses ( search users || display friends || meeting requests )
    private SEARCH_USERS_MODE : number = 0;
    private FRIENDS_MODE: number = 1;
    private REQUEST_MODE: number = 2;

    // default status user search
    private mode: number = 0;

    // search user part
    private users : SearchedUser[];
    private query : string;

    // friends part 
    private sendedRequestUsers: SearchedUser[] = null;
    private receivedRequestUsers: SearchedUser[] = null;
    private friends: SearchedUser[] = null;

    // meeting request part
    private meetingMessage: string = null;
    private meetingFriend: SearchedUser = null;
    private answeringMeetingUser: SearchedUser = null;
    private answeringMeeting: MeetingRequest = null;
    private answeringMessage: string = null;

    // meeting requests
    private sendedRequests: MeetingRequest[] = [];
    private receivedRequests: MeetingRequest[] = [];


    constructor(private userService : AccountService, private router: Router, private friendService: FriendService, 
    private requestService : RequestService ) { }

    ngOnInit() {

        let type = sessionStorage.getItem('page-type');
        this.mode = this.SEARCH_USERS_MODE;

        if (type == 'FRIENDS') {
            this.mode = this.FRIENDS_MODE;
        } 
        else if (type == "REQUESTS") {
            this.mode = this.REQUEST_MODE;
        }

        this.query = localStorage.getItem('user-query');
        this.AuthenticatedUser = Session.AuthenticatedUser;

        this.filterUsers();
        this.loadRequests();
    }

    private loadRequests() {
        this.requestService.GetReceivedRequests(this.AuthenticatedUser.Id).then(requests => {
            console.log(requests);
            this.receivedRequests = requests;
        });

        this.requestService.GetSentRequests(this.AuthenticatedUser.Id).then(requests => {
            //console.log(requests);
            this.sendedRequests = requests;
        });
    }

    private filterUsers() {
        let userId = this.AuthenticatedUser != null ? this.AuthenticatedUser.Id : 0;

        if (this.mode == this.SEARCH_USERS_MODE) {
            this.loadUsersByLogin(userId, this.query);
        }
        else if (this.mode == this.FRIENDS_MODE) {
            this.loadFriends(userId);
        }
    }

    private loadUsersByLogin(userId:number, query: string) {
        this.userService.SearchByLogin(userId, query).then(users => {
             this.users = users;
        });
    }

    private loadFriends(userId:number) {
        this.friendService.GetUserFriends(userId).then(res => {
            this.friends = res.filter(user => user.Relation == UserRelation.Friends);
            this.sendedRequestUsers = res.filter(user => user.Relation == UserRelation.SentRequest);
            this.receivedRequestUsers = res.filter(user => user.Relation == UserRelation.ReceivedRequest);
        });
    }

    private isInReceived(userId: number) {
        return this.receivedRequests.find(req => req.Requester.Id == userId) != null;
    }

    private isInSended(userId: number) {
        return this.sendedRequests.find(req => req.Receiver.Id == userId) != null;
    }

    private isInAccepted(userId:number) {
        return this.sendedRequests.findIndex(req => req.Receiver.Id == userId && req.AnswerText != null) != -1 ||
                this.receivedRequests.findIndex(req => req.Requester.Id == userId && req.AnswerText != null) != -1;
    }

    public searchQuery() {
        this.filterUsers();
    }

    public switchMode(mode: number) {
        this.mode = mode;
        this.filterUsers();

        if (mode == this.SEARCH_USERS_MODE) {
           sessionStorage.removeItem('page-type'); 
        }
        else if (mode == this.FRIENDS_MODE) {
            sessionStorage.setItem('page-type', 'FRIENDS');
        }
        else if (mode == this.REQUEST_MODE) {
            sessionStorage.setItem('page-type', 'REQUESTS');
        }
    }

    public moveToSchedule(user) {
        console.log(user);
        sessionStorage.setItem('schedule-user', JSON.stringify(user));
        this.router.navigate([ '/schedule' ]);
    }

    public addFriend() {

        if (this.AuthenticatedUser == null || this.AuthenticatedUser.Id == this.addingFriend.User.Id) {
            return;
        }

        let friend = new Friend();
        friend.User1Id = this.AuthenticatedUser.Id;
        friend.User2Id = this.addingFriend.User.Id;
        friend.IsAccepted = false;

        this.friendService.CreateFriend(friend).then(res => {
            if (!res.IsSuccess) {
                // error handling
            }
            else {
                this.addingFriend.Relation = UserRelation.SentRequest;
                this.sendedRequestUsers.push(this.addingFriend);
                this.addingFriend = null;
            }
        });
    }

    public answerFriend() {

        if (this.AuthenticatedUser == null || this.AuthenticatedUser.Id == this.answeringFriend.User.Id) {
            return;
        }

        let friend = new Friend();
        friend.User1Id = this.AuthenticatedUser.Id;
        friend.User2Id = this.answeringFriend.User.Id;
        friend.IsAccepted = true;

        this.friendService.UpdateFriend(friend).then(res => {
            if (res.IsSuccess) {
                this.answeringFriend.Relation = UserRelation.Friends;
                
                let ansFriendIndex = this.receivedRequestUsers.findIndex(fr => fr.User.Id == this.answeringFriend.User.Id);
                this.receivedRequestUsers.splice(ansFriendIndex, 1);
                this.friends.push(this.answeringFriend);

                this.answeringFriend = null;
            }
            else {
                // error handling
            }
        });
    }

    public deleteFriend() {
        if (this.AuthenticatedUser == null || this.AuthenticatedUser.Id == this.deletingFriend.User.Id) {
            return;
        }

        this.friendService.DeleteFriend(this.AuthenticatedUser.Id, this.deletingFriend.User.Id).then(res => {
            if (res.IsSuccess) {
                this.deletingFriend.Relation = UserRelation.NoRelation;
                
                let friendIndex = this.friends.findIndex(fr => fr.User.Id == this.deletingFriend.User.Id);
                this.friends.splice(friendIndex, 1);
                
                this.deletingFriend = null;
            }
            else {
                // error handling
            }
        });
    }

    public meetFriend() {
        let request = new MeetingRequest(0, this.AuthenticatedUser, this.meetingFriend.User, this.meetingMessage, null, null);

        this.requestService.CreateRequest(request).then(res => {
            if (res.IsSuccess) {
                this.meetingFriend = null;
                this.loadRequests();
            }
        });
    }

    public answerMeet(user: SearchedUser) {
        let request = this.receivedRequests.find(req => req.Requester.Id == user.User.Id);
        console.log(request);
        this.answeringMeeting = request;
        this.answeringMeetingUser = user;
    }

    public answerMeetWrapper(user: User, relation: number) {
        this.answerMeet(new SearchedUser(user, relation));
    }

    public sendMeetAnswer() {
        let request = this.answeringMeeting;
        request.AnswerText = this.answeringMessage != null ? this.answeringMessage : "";

        this.requestService.UpdateRequest(request).then(res => {
            if (res.IsSuccess) {
                this.answeringMeetingUser = null;
                this.loadRequests();
            }
        })
    }
}