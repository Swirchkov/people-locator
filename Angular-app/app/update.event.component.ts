import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

// Models
import { Event } from "./Models/Event";
import { User } from "./Models/User";

//lib
import { Session } from "./Library/Session";

//Services
import { EventService } from "./Services/event.service";

@Component({
    selector: 'updateEvent',
    templateUrl: '/app/Views/update.event.component.html',
    styleUrls: ['app/Styles/account.css']
})
export class UpdateEventComponent implements OnInit {
    private selectedEvent: Event;
    
    private user : User;

    // validation
    private IsSuccess: boolean = true;
    private ErrorMessage: string = null;

    // form properties
    private Description: string = null;
    private date: Date = new Date();
    private start: Date = new Date();
    private end: Date = new Date();
    private Place: string = null;
    private AvailableTo :number = 0;

    public constructor(private eventService : EventService, private router: Router) { }

    ngOnInit() {
        this.user = Session.AuthenticatedUser;

        if (this.user == null) {
            this.router.navigate(['/login']);
            return;
        }

        let jsonEvent = JSON.parse(localStorage.getItem("edit-Event"));

        if (jsonEvent == null) {
            this.router.navigate(['/schedule']);
            return;
        }
        
        this.selectedEvent = new Event(jsonEvent.id, jsonEvent.description, new Date(jsonEvent.start), 
                    new Date(jsonEvent.end), jsonEvent.place, jsonEvent.userId, jsonEvent.availableTo);
        
        this.Description = this.selectedEvent.Description;
        this.start = this.selectedEvent.Start;
        this.end = this.selectedEvent.End;
        this.date = this.selectedEvent.Start;
        this.Place = this.selectedEvent.Place;
        this.AvailableTo = this.selectedEvent.AvailableTo;
        console.log(this);
    }

    // date binding 
    set Date(e:string) {
        let date = e.split('-');
        let d = new Date(Date.UTC(+date[0], +date[1]-1, +date[2]));
        this.date.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate() + 1);
    }
    get Date() {
        if (this.date == null) {
            return null;
        }
        return this.date.toISOString().substring(0, 10);
    }
    // time start binding
    set Start(e: string) {
        let time = e.split(':');
        this.start.setHours(+time[0]);
        this.start.setMinutes(+time[1]);
    }
    get Start() {
        let hours = this.start.getHours().toString();
        let minutes = this.start.getMinutes().toString();

        hours = hours.length == 1 ? '0' + hours : hours;
        minutes = minutes.length == 1 ? '0' + minutes : minutes;
        return hours + ":" + minutes;
    }

    // time end binding
    set End(e: string) {
        let time = e.split(':');
        this.end.setHours(+time[0]);
        this.end.setMinutes(+time[1]);
    }
    get End() {
        let hours = this.end.getHours().toString();
        let minutes = this.end.getMinutes().toString();
        
        hours = hours.length == 1 ? '0' + hours : hours;
        minutes = minutes.length == 1 ? '0' + minutes : minutes;
        return hours + ":" + minutes;
    }

        private validateForm() {
        let form = <HTMLFormElement>document.getElementsByName('updateEventForm')[0];
        return form.checkValidity();
    }

    public SubmitForm() {
        if (!this.validateForm()) {
            return true;
        }
        
        window.event.preventDefault();

        let start = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(),
                            this.start.getHours(), this.start.getMinutes());
        let end = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(),
                            this.end.getHours(), this.end.getMinutes());

        let event = new Event(this.selectedEvent.Id, this.Description, start, end, this.Place, this.user.Id, this.AvailableTo);

        this.eventService.UpdateEvent(event).then(res => {
            if (res.IsSuccess) {
                this.router.navigate(['/schedule']);
            }
            else {
                this.IsSuccess = false;
                this.ErrorMessage = res.Message;
            }
        });

        return false;
    }
}