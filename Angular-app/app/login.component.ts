/**
 * Created by Vladimir on 29.10.2016.
 */

import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AccountService } from "./Services/account.service";

import { Session } from "./Library/Session";

@Component({
  selector: "login",
  templateUrl: "app/Views/login.component.html",
  styleUrls: [ 'app/Styles/account.css' ]
})
export class LoginComponent {

  private Login = null;
  private Password = null;

  private IsSuccess = true;

  public constructor(private accountService : AccountService,
                      private router : Router) { }

  public SubmitForm() {
    this.accountService.LoginUser(this.Login, this.Password).then((res) => {

      if (res == null) {
        this.IsSuccess = false;
        this.Password = null;
        return;
      }
      
      Session.AuthenticatedUser = res.User;
      Session.SaveToken(res.Token);
      this.router.navigate( ['/schedule'] );

    });
  }
}
