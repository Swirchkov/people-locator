"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var platform_browser_1 = require('@angular/platform-browser');
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var main_component_1 = require('./main.component');
var login_component_1 = require("./login.component");
var register_component_1 = require("./register.component");
var schedule_component_1 = require("./schedule.component");
var create_event_component_1 = require("./create.event.component");
var update_event_component_1 = require("./update.event.component");
var delete_event_component_1 = require("./delete.event.component");
var search_user_component_1 = require("./search.user.component");
//service
var account_service_1 = require("./Services/account.service");
var event_service_1 = require("./Services/event.service");
var friend_service_1 = require("./Services/friend.service");
var request_service_1 = require("./Services/request.service");
// pipes
var time_pipe_1 = require("./Pipes/time.pipe");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                main_component_1.MainComponent,
                login_component_1.LoginComponent,
                app_component_1.AppComponent,
                register_component_1.RegisterComponent,
                schedule_component_1.ScheduleComponent,
                create_event_component_1.CreateEventComponent,
                update_event_component_1.UpdateEventComponent,
                delete_event_component_1.DeleteEventComponent,
                search_user_component_1.SearchUserComponent,
                time_pipe_1.TimePipe
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                app_routing_module_1.AppRoutingModule
            ],
            providers: [account_service_1.AccountService, event_service_1.EventService, friend_service_1.FriendService, request_service_1.RequestService],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map