/**
 * Created by Vladimir on 30.10.2016.
 */

import { User } from "./../Models/User";

export class Session {

  // don't sure about saving data in session storage because we use static session class.
  // it should exist the same time as session. But repeating myself don't sure.

  private static key = "people-locator-user";
  private static user: User = null;
  private static callback: (user:User) => void = null;

  public static get AuthenticatedUser() { return Session.user; }
  public static set AuthenticatedUser(user:User) { 
    Session.user = user;
    Session.callback(user);
  }

  public static SaveToken(token:string) {
    localStorage.setItem(Session.key, token);
  }

  public static GetToken() {
    return localStorage.getItem(Session.key);
  }

  public static RemoveToken() {
    localStorage.removeItem(Session.key);
  }

  public static RegisterCallback(func : (user:User) => void) {
    this.callback = func;
  }
}
