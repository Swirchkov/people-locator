/**
 * Created by Vladimir on 30.10.2016.
 */
"use strict";
var Session = (function () {
    function Session() {
    }
    Object.defineProperty(Session, "AuthenticatedUser", {
        get: function () { return Session.user; },
        set: function (user) {
            Session.user = user;
            Session.callback(user);
        },
        enumerable: true,
        configurable: true
    });
    Session.SaveToken = function (token) {
        localStorage.setItem(Session.key, token);
    };
    Session.GetToken = function () {
        return localStorage.getItem(Session.key);
    };
    Session.RemoveToken = function () {
        localStorage.removeItem(Session.key);
    };
    Session.RegisterCallback = function (func) {
        this.callback = func;
    };
    // don't sure about saving data in session storage because we use static session class.
    // it should exist the same time as session. But repeating myself don't sure.
    Session.key = "people-locator-user";
    Session.user = null;
    Session.callback = null;
    return Session;
}());
exports.Session = Session;
//# sourceMappingURL=Session.js.map