/**
 * Created by User on 30.10.2016.
 */

export class User {
  private id: number;
  private login: string;
  private password: string;
  private company: string;
  private email: string;
  private image : string;

  public constructor(id = 0, login = null, password = null, company = null, email = null, vk = null, facebook = null,
                     image: string = null) {
    this.company = company;
    this.email = email;
    this.id = id;
    this.login = login;
    this.password = password;
    this.image = image;
  }

  public get Id() { return this.id; }

  public get Login() { return this.login; }
  public set Login(value: string) { this.login = value; }

  public get Password() { return this.password; }
  public set Password(value:string) { this.password = value; }

  public get Company() { return this.company; }
  public set Company(value: string) { this.company = value; }

  public get Email() { return this.email; }
  public set Email(value: string) { this.email = value; }

  public get Image() { return this.image; }
  public set Image(value: string) { this.image = value; }

}
