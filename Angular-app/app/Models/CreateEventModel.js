"use strict";
var CreateEventViewModel = (function () {
    function CreateEventViewModel(description, date, start, end, place) {
        if (description === void 0) { description = null; }
        if (date === void 0) { date = null; }
        if (start === void 0) { start = null; }
        if (end === void 0) { end = null; }
        if (place === void 0) { place = null; }
        this.description = description;
        this.date = date;
        this.start = start;
        this.end = end;
        this.place = place;
    }
    Object.defineProperty(CreateEventViewModel.prototype, "Description", {
        get: function () { return this.description; },
        set: function (value) { this.description = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventViewModel.prototype, "Date", {
        get: function () { return this.date; },
        set: function (value) { this.date = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventViewModel.prototype, "Start", {
        get: function () { return this.start; },
        set: function (value) { this.start = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventViewModel.prototype, "End", {
        get: function () { return this.end; },
        set: function (value) { this.end = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventViewModel.prototype, "Place", {
        get: function () { return this.place; },
        set: function (value) { this.place = value; },
        enumerable: true,
        configurable: true
    });
    return CreateEventViewModel;
}());
exports.CreateEventViewModel = CreateEventViewModel;
//# sourceMappingURL=CreateEventModel.js.map