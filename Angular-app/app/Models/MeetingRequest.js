"use strict";
var MeetingRequest = (function () {
    function MeetingRequest(id, requester, receiver, requestText, answerText, date) {
        this.id = id;
        this.requester = requester;
        this.requestText = requestText;
        this.receiver = receiver;
        this.answerText = answerText;
        this.date = date;
    }
    Object.defineProperty(MeetingRequest.prototype, "Id", {
        get: function () { return this.id; },
        set: function (value) { this.id = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MeetingRequest.prototype, "Requester", {
        get: function () { return this.requester; },
        set: function (value) { this.requester = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MeetingRequest.prototype, "Receiver", {
        get: function () { return this.receiver; },
        set: function (value) { this.receiver = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MeetingRequest.prototype, "RequestText", {
        get: function () { return this.requestText; },
        set: function (value) { this.requestText = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MeetingRequest.prototype, "AnswerText", {
        get: function () { return this.answerText; },
        set: function (value) { this.answerText = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MeetingRequest.prototype, "Date", {
        get: function () { return this.date; },
        set: function (value) { this.date = value; },
        enumerable: true,
        configurable: true
    });
    return MeetingRequest;
}());
exports.MeetingRequest = MeetingRequest;
//# sourceMappingURL=MeetingRequest.js.map