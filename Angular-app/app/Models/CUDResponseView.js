/**
 * Created by Vladimir on 30.10.2016.
 */
"use strict";
var CUDResponseView = (function () {
    function CUDResponseView(isSuccess, type, message) {
        if (isSuccess === void 0) { isSuccess = false; }
        if (type === void 0) { type = null; }
        if (message === void 0) { message = null; }
        this.isSuccess = isSuccess;
        this.message = message;
        this.type = type;
    }
    Object.defineProperty(CUDResponseView.prototype, "IsSuccess", {
        get: function () { return this.isSuccess; },
        set: function (value) { this.isSuccess = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CUDResponseView.prototype, "Type", {
        get: function () { return this.type; },
        set: function (value) { this.type = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CUDResponseView.prototype, "Message", {
        get: function () { return this.message; },
        set: function (value) { this.message = value; },
        enumerable: true,
        configurable: true
    });
    return CUDResponseView;
}());
exports.CUDResponseView = CUDResponseView;
//# sourceMappingURL=CUDResponseView.js.map