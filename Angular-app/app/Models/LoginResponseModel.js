/**
 * Created by Vladimir on 30.10.2016.
 */
"use strict";
var LoginResponseModel = (function () {
    function LoginResponseModel(token, user) {
        this.token = token;
        this.user = user;
    }
    Object.defineProperty(LoginResponseModel.prototype, "Token", {
        get: function () { return this.token; },
        set: function (value) { this.token = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginResponseModel.prototype, "User", {
        get: function () { return this.user; },
        set: function (value) { this.user = value; },
        enumerable: true,
        configurable: true
    });
    return LoginResponseModel;
}());
exports.LoginResponseModel = LoginResponseModel;
//# sourceMappingURL=LoginResponseModel.js.map