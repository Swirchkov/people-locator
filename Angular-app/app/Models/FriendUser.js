"use strict";
var FriendUser = (function () {
    function FriendUser(isAccepted, user) {
        this.isAccepted = isAccepted;
        this.user = user;
    }
    Object.defineProperty(FriendUser.prototype, "IsAccepted", {
        get: function () { return this.isAccepted; },
        set: function (value) { this.isAccepted = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FriendUser.prototype, "User", {
        get: function () { return this.user; },
        set: function (value) { this.user = value; },
        enumerable: true,
        configurable: true
    });
    return FriendUser;
}());
exports.FriendUser = FriendUser;
//# sourceMappingURL=FriendUser.js.map