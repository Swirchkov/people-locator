"use strict";
var Friend = (function () {
    function Friend() {
    }
    Object.defineProperty(Friend.prototype, "User1Id", {
        get: function () { return this.user1Id; },
        set: function (value) { this.user1Id = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Friend.prototype, "User2Id", {
        get: function () { return this.user2Id; },
        set: function (value) { this.user2Id = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Friend.prototype, "IsAccepted", {
        get: function () { return this.isAccepted; },
        set: function (value) { this.isAccepted = value; },
        enumerable: true,
        configurable: true
    });
    return Friend;
}());
exports.Friend = Friend;
//# sourceMappingURL=Friend.js.map