export class Friend {
    private user1Id:number;
    private user2Id:number;
    private isAccepted: boolean;

    public constructor() { }

    public get User1Id() { return this.user1Id; }
    public set User1Id(value: number) { this.user1Id = value; }

    public get User2Id() { return this.user2Id; }
    public set User2Id(value: number) { this.user2Id = value; }

    public get IsAccepted() { return this.isAccepted; }
    public set IsAccepted(value: boolean) { this.isAccepted = value; }


}