"use strict";
var SearchedUser = (function () {
    function SearchedUser(user, relation) {
        this.user = user;
        this.relation = relation;
    }
    Object.defineProperty(SearchedUser.prototype, "User", {
        get: function () { return this.user; },
        set: function (value) { this.user = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SearchedUser.prototype, "Relation", {
        get: function () { return this.relation; },
        set: function (value) { this.relation = value; },
        enumerable: true,
        configurable: true
    });
    return SearchedUser;
}());
exports.SearchedUser = SearchedUser;
(function (UserRelation) {
    UserRelation[UserRelation["NoRelation"] = 0] = "NoRelation";
    UserRelation[UserRelation["SentRequest"] = 1] = "SentRequest";
    UserRelation[UserRelation["ReceivedRequest"] = 2] = "ReceivedRequest";
    UserRelation[UserRelation["Friends"] = 3] = "Friends";
})(exports.UserRelation || (exports.UserRelation = {}));
var UserRelation = exports.UserRelation;
;
//# sourceMappingURL=SearchedUser.js.map