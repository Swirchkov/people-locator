"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CUDResponseView_1 = require("./CUDResponseView");
var RegisterResponseModel = (function (_super) {
    __extends(RegisterResponseModel, _super);
    function RegisterResponseModel(isSuccess, type, message, token) {
        if (isSuccess === void 0) { isSuccess = false; }
        if (type === void 0) { type = null; }
        if (message === void 0) { message = null; }
        if (token === void 0) { token = null; }
        _super.call(this, isSuccess, type, message);
        this.token = token;
    }
    Object.defineProperty(RegisterResponseModel.prototype, "Token", {
        get: function () { return this.token; },
        set: function (value) { this.token = value; },
        enumerable: true,
        configurable: true
    });
    return RegisterResponseModel;
}(CUDResponseView_1.CUDResponseView));
exports.RegisterResponseModel = RegisterResponseModel;
//# sourceMappingURL=RegisterResponseModel.js.map