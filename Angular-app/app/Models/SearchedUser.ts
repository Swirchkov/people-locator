import { User } from "./User";

export class SearchedUser {
    private user: User;
    private relation: UserRelation;

    public constructor(user: User, relation: number) {
        this.user = user;
        this.relation = relation;
    }

    public get User() { return this.user; }
    public set User(value: User) { this.user = value; }

    public get Relation() { return this.relation; }
    public set Relation(value: UserRelation) { this.relation = value; }

}

export enum UserRelation {
    NoRelation,
    SentRequest,
    ReceivedRequest,
    Friends
};