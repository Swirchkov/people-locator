import { CUDResponseView } from "./CUDResponseView";

export class RegisterResponseModel extends CUDResponseView {
    private token:string;
    
    public constructor(isSuccess:boolean = false, type: string = null, message: string = null, token: string = null) {
        super(isSuccess, type, message);
        this.token = token;
    }

    public get Token() { return this.token;  }
    public set Token(value:string) { this.token = value; }
    
}