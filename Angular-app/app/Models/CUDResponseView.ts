/**
 * Created by Vladimir on 30.10.2016.
 */

export class CUDResponseView {
  private isSuccess : boolean;
  private type : string;
  private message: string;

  public constructor(isSuccess:boolean = false, type: string = null, message: string = null) {
    this.isSuccess = isSuccess;
    this.message = message;
    this.type = type;
  }

  public get IsSuccess() { return this.isSuccess; }
  public set IsSuccess(value: boolean) { this.isSuccess = value; }

  public get Type() { return this.type; }
  public set Type(value: string) { this.type = value; }

  public get Message() { return this.message; }
  public set Message(value: string) { this.message = value; }

}
