import { User } from "./User"; 

export class MeetingRequest {
    private id:number;
    
    private requester:User;
    private requestText: string;

    private receiver:User;
    private answerText: string;

    private date: Date;

    public constructor(id:number, requester: User, receiver: User, requestText: string, answerText: string, 
            date: Date) {
                this.id = id;
                this.requester = requester;
                this.requestText = requestText;
                this.receiver = receiver;
                this.answerText = answerText;
                this.date = date;
            }

    public get Id() { return this.id; }
    public set Id(value:number) { this.id = value; }

    public get Requester() { return this.requester; }
    public set Requester(value: User) { this.requester = value; }

    public get Receiver() { return this.receiver; }
    public set Receiver(value: User) { this.receiver = value; }

    public get RequestText() { return this.requestText; }
    public set RequestText(value: string) { this.requestText = value; }

    public get AnswerText() { return this.answerText; }
    public set AnswerText(value:string) { this.answerText = value; }

    public get Date() { return this.date; }
    public set Date(value: Date) { this.date = value; }

}