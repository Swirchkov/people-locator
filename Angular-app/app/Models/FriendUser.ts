import { User } from "./User";

export class FriendUser {
    private isAccepted: boolean;
    private user: User;

    public constructor(isAccepted: boolean, user: User) {
        this.isAccepted = isAccepted;
        this.user = user;
    }

    public get IsAccepted() { return this.isAccepted; }
    public set IsAccepted(value: boolean) { this.isAccepted = value; }

    public get User() { return this.user; }
    public set User(value: User) { this.user = value; }
     
}