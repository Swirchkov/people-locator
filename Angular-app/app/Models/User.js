/**
 * Created by User on 30.10.2016.
 */
"use strict";
var User = (function () {
    function User(id, login, password, company, email, vk, facebook, image) {
        if (id === void 0) { id = 0; }
        if (login === void 0) { login = null; }
        if (password === void 0) { password = null; }
        if (company === void 0) { company = null; }
        if (email === void 0) { email = null; }
        if (vk === void 0) { vk = null; }
        if (facebook === void 0) { facebook = null; }
        if (image === void 0) { image = null; }
        this.company = company;
        this.email = email;
        this.id = id;
        this.login = login;
        this.password = password;
        this.image = image;
    }
    Object.defineProperty(User.prototype, "Id", {
        get: function () { return this.id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Login", {
        get: function () { return this.login; },
        set: function (value) { this.login = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Password", {
        get: function () { return this.password; },
        set: function (value) { this.password = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Company", {
        get: function () { return this.company; },
        set: function (value) { this.company = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Email", {
        get: function () { return this.email; },
        set: function (value) { this.email = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "Image", {
        get: function () { return this.image; },
        set: function (value) { this.image = value; },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map