export class Event {
    private id:number = 0;
    private description:string = null;
    private start:Date = null;
    private end: Date = null;
    private place:string = null;
    private userId:number = 0;
    private availableTo:number = 0;

    public constructor(id: number = 0, description: string = null, start:Date = null, end:Date = null, 
                        place:string = null, userId: number = 0, availableTo: number = 0) {
        this.id = id;
        this.description = description;
        this.start = start;
        this.end = end;
        this.place = place;
        this.userId = userId;
        this.availableTo = availableTo;
    }

    public get Id() { return this.id; }
    public set Id(value:number) { this.id = value; }

    public get Description() { return this.description; }
    public set Description(value: string) { this.description = value; }

    public get Start() { return this.start; }
    public set Start(value:Date) { this.start = value; }

    public get End() { return this.end; }
    public set End(value: Date) { this.end = value; }

    public get Place() { return this.place; }
    public set Place(value: string) { this.place = value; }

    public get UserId() { return this.userId; }
    public set UserId(value:number) { this.userId = value; }

    public get AvailableTo() { return this.availableTo; }
    public set AvailableTo(value: number) { this.availableTo = value; } 
}