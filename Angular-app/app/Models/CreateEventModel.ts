export class CreateEventViewModel {
    public constructor( private description: string = null, 
                        private date: Date = null,
                        private start: Date = null,
                        private end : Date = null,
                        private place: string = null) { }
    
    public get Description() { return this.description; }
    public set Description(value: string) { this.description = value; }

    public get Date() { return this.date; }
    public set Date(value: Date) { this.date = value; }

    public get Start() { return this.start; }
    public set Start(value: Date) { this.start = value; }

    public get End() { return this.end; }
    public set End(value: Date) { this.end = value; }

    public get Place() { return this.place; }
    public set Place(value: string) { this.place = value; }    
}