import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "./Models/User";
import { CreateEventViewModel } from "./Models/CreateEventModel";
import { Event } from "./Models/Event";

import { Session } from "./Library/Session";

//services
import { AccountService } from "./Services/account.service";
import { EventService } from "./Services/event.service";

@Component({
    selector: "create-event",
    templateUrl: '/app/Views/create.event.component.html',
    styleUrls: ['app/Styles/account.css']
})
export class CreateEventComponent {

    private user : User;

    // validation
    private IsSuccess: boolean = true;
    private ErrorMessage: string = null;

    // form properties
    private Description: string = null;
    private date: Date = new Date();
    private start: Date = new Date();
    private end: Date = new Date();
    private Place: string = null;
    private AvailableTo: number = 2;

    public constructor(private userService: AccountService, private eventService : EventService,
                        private router : Router) { }

    ngOnInit() {
        this.user = Session.AuthenticatedUser;
        
        if (this.user == null) {
            this.router.navigate([ '/login' ]);
        }

        this.date = new Date(JSON.parse(sessionStorage.getItem('cur-Date')));
    }

    // date binding 
    set Date(e:string) {
        let date = e.split('-');
        let d = new Date(+date[0], +date[1]-1, +date[2]);
        this.date.setFullYear(d.getFullYear(), d.getMonth(), d.getDate() + 1);
    }
    get Date() {
        if (this.date == null) {
            return null;
        }
        return this.date.toISOString().substring(0, 10);
    }
    // time start binding
    set Start(e: string) {
        let time = e.split(':');
        this.start.setHours(+time[0]);
        this.start.setMinutes(+time[1]);
    }
    get Start() {
        let hours = this.start.getHours().toString();
        let minutes = this.start.getMinutes().toString();

        hours = hours.length == 1 ? '0' + hours : hours;
        minutes = minutes.length == 1 ? '0' + minutes : minutes;
        return hours + ":" + minutes;
    }

    // time end binding
    set End(e: string) {
        let time = e.split(':');
        this.end.setHours(+time[0]);
        this.end.setMinutes(+time[1]);
    }
    get End() {
        let hours = this.end.getHours().toString();
        let minutes = this.end.getMinutes().toString();
        
        hours = hours.length == 1 ? '0' + hours : hours;
        minutes = minutes.length == 1 ? '0' + minutes : minutes;
        return hours + ":" + minutes;
    }

    private validateForm() {
        let form = <HTMLFormElement>document.getElementsByName('createEventForm')[0];
        return form.checkValidity();
    }

    public SubmitForm() {
        console.log(this.AvailableTo);
        
        if (!this.validateForm()) {
            return false;
        }
        
        window.event.preventDefault();

        let start = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(),
                            this.start.getHours(), this.start.getMinutes());
        let end = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(),
                            this.end.getHours(), this.end.getMinutes());

        let event = new Event(0, this.Description, start, end, this.Place, this.user.Id, this.AvailableTo);

        console.log(event);

        this.eventService.CreateEvent(event).then(res => {
            if (res.IsSuccess) {
                this.router.navigate(['/schedule']);
            }
            else {
                this.IsSuccess = false;
                this.ErrorMessage = res.Message;
            }
        });

        return false;
    }
}