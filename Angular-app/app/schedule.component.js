"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// services
var account_service_1 = require("./Services/account.service");
var event_service_1 = require("./Services/event.service");
// models
var User_1 = require("./Models/User");
var Event_1 = require("./Models/Event");
var Session_1 = require("./Library/Session");
var ScheduleComponent = (function () {
    function ScheduleComponent(userService, router, eventService) {
        this.userService = userService;
        this.router = router;
        this.eventService = eventService;
        this.events = [];
        this.selectedEvent = new Event_1.Event();
        this.currentDate = new Date();
    }
    ScheduleComponent.prototype.retrieveUser = function (value) {
        var jsParsed = JSON.parse(value);
        return new User_1.User(jsParsed.id, jsParsed.login, jsParsed.password, jsParsed.company, jsParsed.email, jsParsed.vk, jsParsed.facebook, jsParsed.image);
    };
    ScheduleComponent.prototype.ngOnInit = function () {
        this.user = Session_1.Session.AuthenticatedUser;
        var storeValue = sessionStorage.getItem('schedule-user');
        sessionStorage.removeItem('schedule-user');
        if (storeValue != null) {
            this.scheduleUser = this.retrieveUser(storeValue);
        }
        else {
            this.scheduleUser = this.user;
        }
        console.log('Retrieved user');
        console.log(this.scheduleUser);
        if (this.scheduleUser == null) {
            this.router.navigate(['/login']);
            return;
        }
        if (sessionStorage.getItem('cur-Date') != null) {
            this.CurrentDate = new Date(JSON.parse(sessionStorage.getItem('cur-Date')));
        }
        else {
            this.CurrentDate = new Date();
        }
    };
    Object.defineProperty(ScheduleComponent.prototype, "CurrentDate", {
        get: function () {
            return this.currentDate;
        },
        set: function (value) {
            var _this = this;
            this.currentDate = value;
            sessionStorage.setItem('cur-Date', JSON.stringify(this.CurrentDate));
            var userId = this.user != null ? this.user.Id : 0;
            this.eventService.GetEventsByDay(value.getFullYear(), value.getMonth() + 1, value.getDate(), userId, this.scheduleUser.Id).then(function (events) {
                _this.events = events;
            });
        },
        enumerable: true,
        configurable: true
    });
    ScheduleComponent.prototype.selectEvent = function (event) {
        if (this.selectedEvent.Id != event.Id) {
            this.selectedEvent = event;
        }
        else {
            this.selectedEvent = new Event_1.Event();
        }
    };
    ScheduleComponent.prototype.nextDate = function () {
        var date = this.CurrentDate;
        date.setDate(date.getDate() + 1);
        this.CurrentDate = new Date(date.toISOString());
        console.log(this.CurrentDate);
    };
    ScheduleComponent.prototype.prevDate = function () {
        var date = this.CurrentDate;
        date.setDate(date.getDate() - 1);
        this.CurrentDate = new Date(date.toISOString());
        console.log(this.CurrentDate);
    };
    ScheduleComponent.prototype.goToCreateEvent = function () {
        this.router.navigate(['/createEvent']);
    };
    ScheduleComponent.prototype.goToEditEvent = function () {
        localStorage.setItem("edit-Event", JSON.stringify(this.selectedEvent));
        this.router.navigate(['/update-event']);
    };
    ScheduleComponent.prototype.goToDeleteEvent = function () {
        localStorage.setItem("delete-Event", JSON.stringify(this.selectedEvent));
        this.router.navigate(['/delete-event']);
    };
    ScheduleComponent.prototype.moveToSearchUser = function () {
        this.router.navigate(['/userSearch']);
    };
    ScheduleComponent.prototype.moveToFriends = function () {
        sessionStorage.setItem('page-type', "FRIENDS");
        this.router.navigate(['/userSearch']);
    };
    ScheduleComponent.prototype.moveToRequests = function () {
        sessionStorage.setItem('page-type', "REQUESTS");
        this.router.navigate(['/userSearch']);
    };
    ScheduleComponent = __decorate([
        core_1.Component({
            selector: "schedule",
            templateUrl: "/app/Views/schedule.component.html",
            styleUrls: ['app/Styles/schedule.component.css']
        }), 
        __metadata('design:paramtypes', [account_service_1.AccountService, router_1.Router, event_service_1.EventService])
    ], ScheduleComponent);
    return ScheduleComponent;
}());
exports.ScheduleComponent = ScheduleComponent;
//# sourceMappingURL=schedule.component.js.map