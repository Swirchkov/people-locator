/**
 * Created by Vladimir on 30.10.2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var User_1 = require("./../Models/User");
var SearchedUser_1 = require("./../Models/SearchedUser");
var CUDResponseView_1 = require("../Models/CUDResponseView");
var LoginResponseModel_1 = require("../Models/LoginResponseModel");
var RegisterResponseModel_1 = require("../Models/RegisterResponseModel");
var AccountService = (function () {
    function AccountService(http) {
        this.http = http;
        this.path = "http://localhost:24688/api/user";
    }
    AccountService.prototype.transformLoginResponse = function (res) {
        res = res.json();
        if (res == null) {
            return null;
        }
        var token = res.Key;
        var user = new User_1.User(res.Value.Id, res.Value.Login, res.Value.Password, res.Value.Company, res.Value.Email, res.Value.VK, res.Value.Facebook, res.Value.Image);
        return new LoginResponseModel_1.LoginResponseModel(token, user);
    };
    AccountService.prototype.transformToCUDResponse = function (res) {
        res = res.json();
        return new CUDResponseView_1.CUDResponseView(res.IsSuccess, res.Type, res.Message);
    };
    AccountService.prototype.transformToRegisterResponse = function (res) {
        res = res.json();
        return new RegisterResponseModel_1.RegisterResponseModel(res.IsSuccess, res.Type, res.Message, res.Token);
    };
    AccountService.prototype.transformToUserDTO = function (res) {
        return new User_1.User(res.Id, res.Login, res.Password, res.Company, res.Email, res.VK, res.Facebook, "http://localhost:24688" + res.Image);
    };
    AccountService.prototype.transformToUserDTOArray = function (res) {
        res = res.json();
        return res.map(function (user) { return new User_1.User(user.Id, user.Login, user.Password, user.Company, user.Email, user.VK, user.Facebook, "http://localhost:24688" + user.Image); });
    };
    AccountService.prototype.transformToSearchedUserDTO = function (res) {
        return new SearchedUser_1.SearchedUser(this.transformToUserDTO(res.User), res.Relation);
    };
    AccountService.prototype.transformToSearchResult = function (res) {
        var _this = this;
        res = res.json();
        return res.map(function (result) { return _this.transformToSearchedUserDTO(result); });
    };
    AccountService.prototype.LoginUser = function (login, password) {
        var _this = this;
        var headers = new http_1.Headers({ 'content-type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.path + "/login", JSON.stringify({ login: login, password: password }), options)
            .toPromise().then(function (res) { return _this.transformLoginResponse(res); });
    };
    AccountService.prototype.RegisterUser = function (user, image) {
        var _this = this;
        var formData = new FormData();
        formData.append('Login', user.Login);
        formData.append('Password', user.Password);
        formData.append('Email', user.Email);
        formData.append('Company', user.Company);
        formData.append('Image', image);
        return this.http.post(this.path + "/register", formData).toPromise().then(function (res) { return _this.transformToRegisterResponse(res); });
    };
    AccountService.prototype.ValidateToken = function (token) {
        var _this = this;
        return this.http.get(this.path + "/validatetoken?token=" + encodeURIComponent(token)).toPromise()
            .then(function (res) { return _this.transformToUserDTO(res.json()); });
    };
    AccountService.prototype.SearchByLogin = function (userId, query) {
        var _this = this;
        return this.http.get(this.path + "?userId=" + userId + "&query=" + query).toPromise()
            .then(function (res) { return _this.transformToSearchResult(res); });
    };
    AccountService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AccountService);
    return AccountService;
}());
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map