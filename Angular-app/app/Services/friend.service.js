"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var SearchedUser_1 = require("./../Models/SearchedUser");
var User_1 = require("./../Models/User");
var CUDResponseView_1 = require("./../Models/CUDResponseView");
var FriendService = (function () {
    function FriendService(http) {
        this.http = http;
        this.path = "http://localhost:24688/";
    }
    FriendService.prototype.transformToCUDResponse = function (res) {
        return new CUDResponseView_1.CUDResponseView(res.IsSuccess, res.Type, res.Message);
    };
    FriendService.prototype.mapToUser = function (res) {
        return new User_1.User(res.Id, res.Login, res.Password, res.Company, res.Email, res.VK, res.Facebook, "http://localhost:24688" + res.Image);
    };
    FriendService.prototype.mapToFriendUserArray = function (res) {
        //return res.map(fu => new FriendUser(fu.IsAccepted, this.mapToUser(fu.User)));
    };
    FriendService.prototype.mapToSearchedUserArray = function (res) {
        var _this = this;
        return res.map(function (su) { return new SearchedUser_1.SearchedUser(_this.mapToUser(su.User), su.Relation); });
    };
    FriendService.prototype.GetUserFriends = function (userId) {
        var _this = this;
        return this.http.get(this.path + "api/friends/getUserFriends?userId=" + userId).toPromise().then(function (res) {
            return _this.mapToSearchedUserArray(res.json());
        });
    };
    FriendService.prototype.CreateFriend = function (friend) {
        var _this = this;
        return this.http.post(this.path + "api/friend", friend).toPromise().then(function (res) {
            return _this.transformToCUDResponse(res.json());
        });
    };
    FriendService.prototype.UpdateFriend = function (friend) {
        var _this = this;
        return this.http.put(this.path + "api/friend", friend).toPromise().then(function (res) {
            return _this.transformToCUDResponse(res.json());
        });
    };
    FriendService.prototype.DeleteFriend = function (user1Id, user2Id) {
        var _this = this;
        return this.http.delete(this.path + "api/friend?user1Id=" + user1Id + "&user2Id=" + user2Id).toPromise().then(function (res) {
            return _this.transformToCUDResponse(res.json());
        });
    };
    FriendService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], FriendService);
    return FriendService;
}());
exports.FriendService = FriendService;
//# sourceMappingURL=friend.service.js.map