"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var MeetingRequest_1 = require("./../Models/MeetingRequest");
var CUDResponseView_1 = require("./../Models/CUDResponseView");
var User_1 = require("./../Models/User");
var RequestService = (function () {
    function RequestService(http) {
        this.http = http;
        this.path = "http://localhost:24688/";
    }
    RequestService.prototype.transformToCUDResponse = function (res) {
        return new CUDResponseView_1.CUDResponseView(res.IsSuccess, res.Type, res.Message);
    };
    RequestService.prototype.mapToUser = function (res) {
        return new User_1.User(res.Id, res.Login, res.Password, res.Company, res.Email, res.VK, res.Facebook, "http://localhost:24688" + res.Image);
    };
    RequestService.prototype.mapToMeetingRequest = function (res) {
        return new MeetingRequest_1.MeetingRequest(res.Id, this.mapToUser(res.Requester), this.mapToUser(res.Receiver), res.RequestText, res.AnswerText, res.Date);
    };
    RequestService.prototype.mapToMeetingRequestArray = function (res) {
        var _this = this;
        return res.map(function (req) { return _this.mapToMeetingRequest(req); });
    };
    RequestService.prototype.CreateRequest = function (req) {
        var _this = this;
        var sendObj = {
            Id: req.Id,
            ReceiverId: req.Receiver.Id,
            RequesterId: req.Requester.Id,
            RequestText: req.RequestText,
            AnswerText: req.AnswerText,
            FinishDate: req.Date
        };
        return this.http.post(this.path + "api/request", sendObj).toPromise().then(function (res) {
            return _this.transformToCUDResponse(res.json());
        });
    };
    RequestService.prototype.UpdateRequest = function (req) {
        var _this = this;
        var sendObj = {
            Id: req.Id,
            ReceiverId: req.Receiver.Id,
            RequesterId: req.Requester.Id,
            RequestText: req.RequestText,
            AnswerText: req.AnswerText,
            FinishDate: req.Date
        };
        return this.http.put(this.path + "api/request", sendObj).toPromise().then(function (res) {
            return _this.transformToCUDResponse(res.json());
        });
    };
    RequestService.prototype.DeleteRequest = function (id) {
        var _this = this;
        return this.http.delete(this.path + "api/request?id=" + id).toPromise().then(function (res) {
            return _this.transformToCUDResponse(res.json());
        });
    };
    RequestService.prototype.GetReceivedRequests = function (userId) {
        var _this = this;
        return this.http.get(this.path + "api/request/received?userId=" + userId).toPromise().then(function (res) {
            return _this.mapToMeetingRequestArray(res.json());
        });
    };
    RequestService.prototype.GetSentRequests = function (userId) {
        var _this = this;
        return this.http.get(this.path + "api/request/sended?userId=" + userId).toPromise().then(function (res) {
            return _this.mapToMeetingRequestArray(res.json());
        });
    };
    RequestService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], RequestService);
    return RequestService;
}());
exports.RequestService = RequestService;
//# sourceMappingURL=request.service.js.map