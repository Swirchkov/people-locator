"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Event_1 = require("./../Models/Event");
var CUDResponseView_1 = require("./../Models/CUDResponseView");
require("rxjs/add/operator/toPromise");
var EventService = (function () {
    function EventService(http) {
        this.http = http;
        this.url = "http://localhost:24688/api/event";
    }
    EventService.prototype.transformToCUDResponse = function (res) {
        res = res.json();
        return new CUDResponseView_1.CUDResponseView(res.IsSuccess, res.Type, res.Message);
    };
    EventService.prototype.transformToEventsArray = function (res) {
        res = res.json();
        return res.map(function (ev) { return new Event_1.Event(ev.EventId, ev.Description, new Date(ev.Start), new Date(ev.End), ev.Place, ev.UserId, ev.AvailableTo); });
    };
    EventService.prototype.CreateEvent = function (event) {
        var _this = this;
        return this.http.post(this.url, event).toPromise().then(function (res) { return _this.transformToCUDResponse(res); });
    };
    EventService.prototype.UpdateEvent = function (event) {
        var _this = this;
        var putObj = {
            EventId: event.Id,
            Description: event.Description,
            Start: event.Start,
            End: event.End,
            Place: event.Place,
            UserId: event.UserId,
            AvailableTo: event.AvailableTo
        };
        return this.http.put(this.url, putObj).toPromise().then(function (res) { return _this.transformToCUDResponse(res); });
    };
    EventService.prototype.DeleteEvent = function (id) {
        var _this = this;
        return this.http.delete(this.url + "?id=" + id).toPromise().then(function (res) { return _this.transformToCUDResponse(res); });
    };
    EventService.prototype.GetEvents = function () {
        var _this = this;
        return this.http.get(this.url).toPromise().then(function (res) { return _this.transformToEventsArray(res); });
    };
    EventService.prototype.GetEventsByDay = function (year, month, date, userId, scheduleUserId) {
        var _this = this;
        return this.http.get(this.url + "/eventsByDay?year=" + year + "&month=" + month + "&day=" + date +
            "&userId=" + userId + "&scheduleUserId=" + scheduleUserId).toPromise()
            .then(function (res) { return _this.transformToEventsArray(res); });
    };
    EventService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], EventService);
    return EventService;
}());
exports.EventService = EventService;
//# sourceMappingURL=event.service.js.map