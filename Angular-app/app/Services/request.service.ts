import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { MeetingRequest } from "./../Models/MeetingRequest";
import { CUDResponseView } from "./../Models/CUDResponseView";
import { User } from "./../Models/User";

@Injectable()
export class RequestService {
    private path: string = "http://localhost:24688/";

    public constructor(private http: Http) { }

    private transformToCUDResponse(res) {
        return new CUDResponseView(res.IsSuccess, res.Type, res.Message);
    }

    private mapToUser(res : any) : User {
        return new User(res.Id, res.Login, res.Password, res.Company, res.Email, res.VK, res.Facebook, 
            "http://localhost:24688" + res.Image);
    }

    private mapToMeetingRequest(res) {
        return new MeetingRequest(res.Id, this.mapToUser(res.Requester), this.mapToUser(res.Receiver), res.RequestText, 
            res.AnswerText, res.Date);
    }

    private mapToMeetingRequestArray(res) {
        return res.map(req => this.mapToMeetingRequest(req));
    }

    public CreateRequest(req: MeetingRequest) {
        let sendObj = {
            Id: req.Id,
            ReceiverId: req.Receiver.Id,
            RequesterId: req.Requester.Id,
            RequestText: req.RequestText,
            AnswerText: req.AnswerText,
            FinishDate: req.Date
        };

        return this.http.post(this.path + "api/request", sendObj).toPromise().then(res => {
            return this.transformToCUDResponse(res.json());
        });
    }

    public UpdateRequest(req: MeetingRequest) {
        let sendObj = {
            Id: req.Id,
            ReceiverId: req.Receiver.Id,
            RequesterId: req.Requester.Id,
            RequestText: req.RequestText,
            AnswerText: req.AnswerText,
            FinishDate: req.Date
        };

        return this.http.put(this.path + "api/request", sendObj).toPromise().then(res => {
            return this.transformToCUDResponse(res.json());
        });
    }

    public DeleteRequest(id: number) {
        return this.http.delete(this.path + "api/request?id=" + id).toPromise().then(res => {
            return this.transformToCUDResponse(res.json());
        });
    }

    public GetReceivedRequests(userId: number) {
        return this.http.get(this.path + "api/request/received?userId=" + userId).toPromise().then(res => {
            return this.mapToMeetingRequestArray(res.json());
        });
    }
    
    public GetSentRequests(userId: number) {
        return this.http.get(this.path + "api/request/sended?userId=" + userId).toPromise().then(res => {
            return this.mapToMeetingRequestArray(res.json());
        });
    }
}