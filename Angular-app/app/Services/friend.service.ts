import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { Friend } from "./../Models/Friend";
import { SearchedUser } from "./../Models/SearchedUser";
import { User } from "./../Models/User";
import { CUDResponseView } from "./../Models/CUDResponseView";

@Injectable()
export class FriendService {
    private path:string = "http://localhost:24688/";

    private transformToCUDResponse(res) {
        return new CUDResponseView(res.IsSuccess, res.Type, res.Message);
    }

    public constructor(private http: Http) { }

    private mapToUser(res) : User {
        return new User(res.Id, res.Login, res.Password, res.Company, res.Email, res.VK, res.Facebook, 
            "http://localhost:24688" + res.Image);
    }

    private mapToFriendUserArray(res) {
        //return res.map(fu => new FriendUser(fu.IsAccepted, this.mapToUser(fu.User)));
    }

    private mapToSearchedUserArray(res: any) {
        return res.map( su => new SearchedUser(this.mapToUser(su.User), su.Relation) );
    }


    public GetUserFriends(userId: number) : Promise<SearchedUser[]> {
        return this.http.get(this.path + "api/friends/getUserFriends?userId=" + userId).toPromise().then(res => {
            return this.mapToSearchedUserArray(res.json());
        });
    }

    public CreateFriend(friend: Friend) {
        return this.http.post(this.path + "api/friend", friend).toPromise().then(res => {
            return this.transformToCUDResponse(res.json());
        });
    }

    public UpdateFriend(friend: Friend) {
        return this.http.put(this.path + "api/friend", friend).toPromise().then(res => {
            return this.transformToCUDResponse(res.json());
        });
    }

    public DeleteFriend(user1Id: number, user2Id: number) {
        return this.http.delete(this.path + "api/friend?user1Id=" + user1Id + "&user2Id=" + user2Id).toPromise().then(res => {
            return this.transformToCUDResponse(res.json());
        });
    }
}