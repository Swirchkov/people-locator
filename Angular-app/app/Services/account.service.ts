/**
 * Created by Vladimir on 30.10.2016.
 */

import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from "@angular/http";

import "rxjs/add/operator/toPromise";

import { User } from "./../Models/User";
import { SearchedUser } from "./../Models/SearchedUser";
import { CUDResponseView } from "../Models/CUDResponseView";
import { LoginResponseModel } from "../Models/LoginResponseModel";
import { RegisterResponseModel } from "../Models/RegisterResponseModel";

@Injectable()
export class AccountService {

  private path = "http://localhost:24688/api/user";

  public constructor(private http: Http) { }

  private transformLoginResponse(res: any) : LoginResponseModel {
    res = res.json();

    if (res == null) {
      return null;
    }

    var token = res.Key;
    var user : User = new User(res.Value.Id, res.Value.Login, res.Value.Password, res.Value.Company, res.Value.Email, res.Value.VK,
        res.Value.Facebook, res.Value.Image);
        
    return new LoginResponseModel(token, user);
  }

  private transformToCUDResponse(res: any) : CUDResponseView {
    res = res.json();
    return new CUDResponseView(res.IsSuccess, res.Type, res.Message);
  }

  private transformToRegisterResponse(res: any) : RegisterResponseModel {
    res = res.json();
    return new RegisterResponseModel(res.IsSuccess, res.Type, res.Message, res.Token);
  }

  private transformToUserDTO(res : any) : User {
    return new User(res.Id, res.Login, res.Password, res.Company, res.Email, res.VK, res.Facebook, 
      "http://localhost:24688" + res.Image);
  }

  private transformToUserDTOArray(res: any) {
    res = res.json();
    return res.map(user => new User(user.Id, user.Login, user.Password, user.Company, user.Email, user.VK, user.Facebook,
                        "http://localhost:24688" + user.Image));
  }

  private transformToSearchedUserDTO(res: any) {
    return new SearchedUser(this.transformToUserDTO(res.User), res.Relation);
  } 

  private transformToSearchResult(res: any) {
    res = res.json();
    return res.map(result => this.transformToSearchedUserDTO(result));
  } 

  public LoginUser(login:string, password: string) : Promise<LoginResponseModel> {

    let headers = new Headers({ 'content-type' : 'application/json' });
    let options = new RequestOptions( { headers: headers } );

    return this.http.post(this.path + "/login", JSON.stringify({ login : login, password : password }), options)
      .toPromise().then(res => this.transformLoginResponse(res));
  }

  public RegisterUser(user:User, image: File) {

    let formData = new FormData();
    formData.append('Login', user.Login);
    formData.append('Password', user.Password);
    formData.append('Email', user.Email);
    formData.append('Company', user.Company);
    formData.append('Image', image);

    return this.http.post(this.path + "/register", formData).toPromise().then(res => this.transformToRegisterResponse(res));
  }

  public ValidateToken(token:string) {
    return this.http.get(this.path + "/validatetoken?token=" + encodeURIComponent(token)).toPromise()
          .then(res => this.transformToUserDTO(res.json()));
  }

  
  public SearchByLogin(userId:number, query : string) {
    return this.http.get(this.path + "?userId=" + userId + "&query=" + query).toPromise()
        .then(res => this.transformToSearchResult(res));
  }

}
