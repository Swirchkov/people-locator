import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { Event } from "./../Models/Event";
import { CUDResponseView } from "./../Models/CUDResponseView";
import "rxjs/add/operator/toPromise";

@Injectable()
export class EventService {
    private url = "http://localhost:24688/api/event";

    constructor(private http: Http) { }

    private transformToCUDResponse(res: any): CUDResponseView {
        res = res.json();
        return new CUDResponseView(res.IsSuccess, res.Type, res.Message);
    }

    private transformToEventsArray(res: any) : Event[] {
        res = res.json();

        return res.map(ev => new Event(ev.EventId, ev.Description , new Date(ev.Start), new Date(ev.End), ev.Place,
                                ev.UserId, ev.AvailableTo));
    }

    public CreateEvent(event: Event) {
        return this.http.post(this.url, event).toPromise().then(res => this.transformToCUDResponse(res));
    }

    public UpdateEvent(event: Event) {
        let putObj = {
            EventId : event.Id,
            Description: event.Description,
            Start: event.Start,
            End: event.End,
            Place: event.Place,
            UserId: event.UserId,
            AvailableTo: event.AvailableTo
        };

        return this.http.put(this.url, putObj).toPromise().then(res => this.transformToCUDResponse(res));
    }

    public DeleteEvent(id: number) {
        return this.http.delete(this.url + "?id=" + id).toPromise().then(res => this.transformToCUDResponse(res));
    }

    public GetEvents() : Promise<Event[]> {
        return this.http.get(this.url).toPromise().then(res => this.transformToEventsArray(res));
    }

    public GetEventsByDay(year: number, month: number, date: number, userId: number, scheduleUserId: number) {
        return this.http.get(this.url + "/eventsByDay?year=" + year + "&month=" + month + "&day=" + date + 
        "&userId=" + userId + "&scheduleUserId=" + scheduleUserId).toPromise()
            .then(res => this.transformToEventsArray(res));
    }
}