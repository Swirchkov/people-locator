import { Pipe, PipeTransform } from "@angular/core"; 

@Pipe({
    name: 'time'
})
export class TimePipe implements PipeTransform {

    private to2DigitValue(value:number) {
        if (value < 10) {
            return "0" + value.toString();
        }
        return value.toString();
    }

    transform(value: Date, args?: any) : string {
        return this.to2DigitValue(value.getHours()) + ":" + this.to2DigitValue(value.getMinutes());
    }
}