import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from "./app-routing.module";

import { AppComponent } from "./app.component";

import { MainComponent } from './main.component';

import { LoginComponent } from "./login.component";
import { RegisterComponent } from "./register.component";

import { ScheduleComponent } from "./schedule.component";

import { CreateEventComponent } from "./create.event.component";
import { UpdateEventComponent } from "./update.event.component";
import { DeleteEventComponent } from "./delete.event.component";

import { SearchUserComponent } from "./search.user.component";

//service
import { AccountService } from "./Services/account.service";
import { EventService } from "./Services/event.service";
import { FriendService } from "./Services/friend.service";
import { RequestService } from "./Services/request.service";

// pipes
import { TimePipe } from "./Pipes/time.pipe";

@NgModule({
  declarations: [
    MainComponent,
    LoginComponent,
    AppComponent,
    RegisterComponent,
    ScheduleComponent,
    CreateEventComponent,
    UpdateEventComponent,
    DeleteEventComponent,
    SearchUserComponent,
    TimePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [ AccountService, EventService, FriendService, RequestService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
