"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by Vladimir on 29.10.2016.
 */
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//service
var account_service_1 = require("./Services/account.service");
//lib
var Session_1 = require("./Library/Session");
var AppComponent = (function () {
    function AppComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.user = null;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (Session_1.Session.AuthenticatedUser == null) {
            var token = Session_1.Session.GetToken();
            if (token != null) {
                this.userService.ValidateToken(token).then(function (user) {
                    _this.user = user;
                    Session_1.Session.AuthenticatedUser = user;
                    if (user != null) {
                        _this.router.navigate(['/schedule']);
                    }
                });
            }
            else {
                this.router.navigate(['/main']);
            }
        }
        else {
            this.user = Session_1.Session.AuthenticatedUser;
            this.router.navigate(['/schedule']);
        }
        Session_1.Session.RegisterCallback(function (user) { return setTimeout(_this.onUserChanged(user), 1); });
    };
    AppComponent.prototype.onUserChanged = function (user) {
        this.user = user;
        if (user == null) {
            this.router.navigate(['/main']);
        }
        else {
            this.router.navigate(['/schedule']);
        }
    };
    AppComponent.prototype.LogOut = function () {
        this.user = null;
        Session_1.Session.AuthenticatedUser = null;
        Session_1.Session.RemoveToken();
        this.router.navigate(['/main']);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "app-root",
            templateUrl: "/app/Views/layout.html",
            styleUrls: ['app/Styles/layout.css']
        }), 
        __metadata('design:paramtypes', [account_service_1.AccountService, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map