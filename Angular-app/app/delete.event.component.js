"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//Services
var event_service_1 = require("./Services/event.service");
// Models
var Event_1 = require("./Models/Event");
//lib
var Session_1 = require("./Library/Session");
var DeleteEventComponent = (function () {
    function DeleteEventComponent(router, eventService) {
        this.router = router;
        this.eventService = eventService;
    }
    DeleteEventComponent.prototype.ngOnInit = function () {
        this.user = Session_1.Session.AuthenticatedUser;
        if (this.user == null) {
            this.router.navigate(['/login']);
            return;
        }
        var jsonEvent = JSON.parse(localStorage.getItem("delete-Event"));
        if (jsonEvent == null) {
            this.router.navigate(['/schedule']);
            return;
        }
        this.event = new Event_1.Event(jsonEvent.id, jsonEvent.description, new Date(jsonEvent.start), new Date(jsonEvent.end), jsonEvent.place, jsonEvent.userId);
    };
    DeleteEventComponent.prototype.delete = function () {
        var _this = this;
        this.eventService.DeleteEvent(this.event.Id).then(function (res) {
            if (res.IsSuccess) {
                _this.router.navigate(['/schedule']);
            }
            else {
                console.warn(res.Message);
            }
        });
    };
    DeleteEventComponent.prototype.cancel = function () {
        this.router.navigate(['/schedule']);
    };
    DeleteEventComponent = __decorate([
        core_1.Component({
            selector: 'delete-event',
            templateUrl: "/app/Views/delete.event.component.html",
            styleUrls: ['app/Styles/schedule.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, event_service_1.EventService])
    ], DeleteEventComponent);
    return DeleteEventComponent;
}());
exports.DeleteEventComponent = DeleteEventComponent;
//# sourceMappingURL=delete.event.component.js.map