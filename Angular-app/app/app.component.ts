/**
 * Created by Vladimir on 29.10.2016.
 */
import { Component } from "@angular/core";
import { Router } from "@angular/router";

// models
import { User } from "./Models/User";

//service
import { AccountService } from "./Services/account.service";

//lib
import { Session } from "./Library/Session";


@Component({
  selector: "app-root",
  templateUrl: "/app/Views/layout.html",
  styleUrls: [ 'app/Styles/layout.css' ]
})
export class AppComponent {
  private user: User = null;

  constructor(private userService: AccountService, private router : Router) { }

  ngOnInit() {
    if (Session.AuthenticatedUser == null) {
      var token = Session.GetToken();

      if (token != null) {
        this.userService.ValidateToken(token).then(user => {
          this.user = user;
          Session.AuthenticatedUser = user;

          if (user != null) {
            this.router.navigate(['/schedule']);
          }
        });
      } 
      else {
        this.router.navigate(['/main']);
      }
    }
    else {
      this.user = Session.AuthenticatedUser;
      this.router.navigate(['/schedule']);
    }

    Session.RegisterCallback(user => setTimeout(this.onUserChanged(user), 1));
  }

  private onUserChanged(user:User) {
    this.user = user;

    if (user == null) {
      this.router.navigate(['/main']);
    }
    else {
      this.router.navigate(['/schedule']);
    }
  }

  LogOut() {
    this.user = null;
    Session.AuthenticatedUser = null;
    Session.RemoveToken();
    this.router.navigate(['/main']);
  }
}
