import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

// services
import { AccountService } from "./Services/account.service";
import { EventService } from "./Services/event.service";

// models
import { User } from "./Models/User";
import { Event } from "./Models/Event";

import { Session } from "./Library/Session";

@Component({
    selector: "schedule",
    templateUrl: "/app/Views/schedule.component.html",
    styleUrls: ['app/Styles/schedule.component.css']
})
export class ScheduleComponent implements OnInit {
    private user: User;
    private scheduleUser : User;
    private events: Event[] = [];
    private selectedEvent: Event = new Event();
    private currentDate: Date = new Date();

    public constructor(private userService: AccountService, private router : Router, private eventService: EventService) 
    { }

    private retrieveUser(value: string) : User {
        let jsParsed = JSON.parse(value);
        return new User(jsParsed.id, jsParsed.login, jsParsed.password, jsParsed.company, jsParsed.email, jsParsed.vk,
                        jsParsed.facebook, jsParsed.image);
    } 

    ngOnInit() {
        this.user = Session.AuthenticatedUser;
        let storeValue = sessionStorage.getItem('schedule-user');
        sessionStorage.removeItem('schedule-user');

        if (storeValue != null) {
            this.scheduleUser = this.retrieveUser(storeValue);
        }
        else {
            this.scheduleUser = this.user;
        }

        console.log('Retrieved user');
        console.log(this.scheduleUser);

        if (this.scheduleUser == null) {
            this.router.navigate([ '/login' ]);
            return;
        }

        if (sessionStorage.getItem('cur-Date') != null) {
            this.CurrentDate = new Date(JSON.parse(sessionStorage.getItem('cur-Date')));
        }
        else { 
            this.CurrentDate = new Date();
        }
    }

    get CurrentDate() {
        return this.currentDate;
    }
    set CurrentDate(value: Date) {
        this.currentDate = value;
        sessionStorage.setItem('cur-Date', JSON.stringify(this.CurrentDate));

        let userId = this.user != null ? this.user.Id : 0;

        this.eventService.GetEventsByDay(value.getFullYear(), value.getMonth() + 1, value.getDate(), userId, 
            this.scheduleUser.Id).then(events => { 
                this.events = events; 
            });

    }

    public selectEvent(event: Event) {
        if (this.selectedEvent.Id != event.Id) {
            this.selectedEvent = event;
        }
        else {
            this.selectedEvent = new Event();
        }
    }

    public nextDate() {
        let date = this.CurrentDate;
        date.setDate(date.getDate() + 1);
        this.CurrentDate = new Date(date.toISOString());

        console.log(this.CurrentDate);
    }

    public prevDate() {
        let date = this.CurrentDate;
        date.setDate(date.getDate() - 1);
        this.CurrentDate = new Date(date.toISOString());

        console.log(this.CurrentDate);
    }

    public goToCreateEvent() {
        this.router.navigate([ '/createEvent' ]);
    }

    public goToEditEvent() {
        localStorage.setItem("edit-Event", JSON.stringify(this.selectedEvent));

        this.router.navigate(['/update-event']);
    }

    public goToDeleteEvent() {
        localStorage.setItem("delete-Event", JSON.stringify(this.selectedEvent));

        this.router.navigate(['/delete-event']);
    }

    public moveToSearchUser() {
        this.router.navigate(['/userSearch']);
    }

    public moveToFriends() {
        sessionStorage.setItem('page-type', "FRIENDS");
        this.router.navigate(['/userSearch']);
    }
    
    public moveToRequests() {
        sessionStorage.setItem('page-type', "REQUESTS");
        this.router.navigate(['/userSearch']);
    }
}