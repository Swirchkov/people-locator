/**
 * Created by Vladimir on 30.10.2016.
 */

import { Component } from "@angular/core";
import { Router } from "@angular/router";
import {User} from "./Models/User";
import {AccountService} from "./Services/account.service";
import {Session} from "./Library/Session";

@Component({
  selector: "register",
  templateUrl: "app/Views/register.component.html",
  styleUrls : [ 'app/Styles/account.css' ]
})
export class RegisterComponent {
  private hasErrors: boolean = false;

  private user:User = new User();

  public constructor(private accountService: AccountService,
                     private router : Router) { }

  public validateImage() {
    // check image file from input 
    let inputElement = <HTMLInputElement> document.getElementsByName('Image')[0];
    let file = inputElement.files[0];

    if (!file.name.endsWith('jpg') && !file.name.endsWith('jpeg') && !file.name.endsWith('png')) {
      inputElement.setCustomValidity("Invalid file format");
      console.log('invalid');
    }
    else {
      console.log('valid');
      //reset custom validity
      inputElement.setCustomValidity("");
    }
    console.log(inputElement.validity);
  }

  private validateForm() : boolean {
    let form : HTMLFormElement = document.getElementsByName('registerForm')[0] as HTMLFormElement;

    let validity = form.checkValidity();
    
    if (!validity) { return validity; }

    return validity;
  }

  private getImageFile() : File {
    let inputElement = <HTMLInputElement> document.getElementsByName('Image')[0];
    return inputElement.files[0];
  }

  public Register() {
    window.event.preventDefault();

    if (!this.validateForm()) {
      return false;
    }

    this.accountService.RegisterUser(this.user, this.getImageFile()).then(res => {
      
      if (res.IsSuccess) {
        
        Session.AuthenticatedUser = this.user;
        Session.SaveToken(res.Token);

        this.router.navigate([ '/schedule' ]);
      }
      else {
        this.hasErrors = true;
      }
    });

    return false;
  }

}
