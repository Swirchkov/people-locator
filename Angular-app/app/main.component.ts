import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { Session } from "./Library/Session";
import { User } from "./Models/User";

import { AccountService } from "./Services/account.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app/Views/main.component.html',
  styleUrls: ['app/Styles/main.component.css']
})
export class MainComponent {
  private searching: boolean = false;
  private query: string = null;

  constructor(private router : Router) { }

  public searchQuery() {
    localStorage.setItem('user-query', this.query);
    this.router.navigate([ '/userSearch' ]);
  }
}
