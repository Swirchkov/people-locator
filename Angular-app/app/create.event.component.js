"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Event_1 = require("./Models/Event");
var Session_1 = require("./Library/Session");
//services
var account_service_1 = require("./Services/account.service");
var event_service_1 = require("./Services/event.service");
var CreateEventComponent = (function () {
    function CreateEventComponent(userService, eventService, router) {
        this.userService = userService;
        this.eventService = eventService;
        this.router = router;
        // validation
        this.IsSuccess = true;
        this.ErrorMessage = null;
        // form properties
        this.Description = null;
        this.date = new Date();
        this.start = new Date();
        this.end = new Date();
        this.Place = null;
        this.AvailableTo = 2;
    }
    CreateEventComponent.prototype.ngOnInit = function () {
        this.user = Session_1.Session.AuthenticatedUser;
        if (this.user == null) {
            this.router.navigate(['/login']);
        }
        this.date = new Date(JSON.parse(sessionStorage.getItem('cur-Date')));
    };
    Object.defineProperty(CreateEventComponent.prototype, "Date", {
        get: function () {
            if (this.date == null) {
                return null;
            }
            return this.date.toISOString().substring(0, 10);
        },
        // date binding 
        set: function (e) {
            var date = e.split('-');
            var d = new Date(+date[0], +date[1] - 1, +date[2]);
            this.date.setFullYear(d.getFullYear(), d.getMonth(), d.getDate() + 1);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "Start", {
        get: function () {
            var hours = this.start.getHours().toString();
            var minutes = this.start.getMinutes().toString();
            hours = hours.length == 1 ? '0' + hours : hours;
            minutes = minutes.length == 1 ? '0' + minutes : minutes;
            return hours + ":" + minutes;
        },
        // time start binding
        set: function (e) {
            var time = e.split(':');
            this.start.setHours(+time[0]);
            this.start.setMinutes(+time[1]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "End", {
        get: function () {
            var hours = this.end.getHours().toString();
            var minutes = this.end.getMinutes().toString();
            hours = hours.length == 1 ? '0' + hours : hours;
            minutes = minutes.length == 1 ? '0' + minutes : minutes;
            return hours + ":" + minutes;
        },
        // time end binding
        set: function (e) {
            var time = e.split(':');
            this.end.setHours(+time[0]);
            this.end.setMinutes(+time[1]);
        },
        enumerable: true,
        configurable: true
    });
    CreateEventComponent.prototype.validateForm = function () {
        var form = document.getElementsByName('createEventForm')[0];
        return form.checkValidity();
    };
    CreateEventComponent.prototype.SubmitForm = function () {
        var _this = this;
        console.log(this.AvailableTo);
        if (!this.validateForm()) {
            return false;
        }
        window.event.preventDefault();
        var start = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(), this.start.getHours(), this.start.getMinutes());
        var end = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(), this.end.getHours(), this.end.getMinutes());
        var event = new Event_1.Event(0, this.Description, start, end, this.Place, this.user.Id, this.AvailableTo);
        console.log(event);
        this.eventService.CreateEvent(event).then(function (res) {
            if (res.IsSuccess) {
                _this.router.navigate(['/schedule']);
            }
            else {
                _this.IsSuccess = false;
                _this.ErrorMessage = res.Message;
            }
        });
        return false;
    };
    CreateEventComponent = __decorate([
        core_1.Component({
            selector: "create-event",
            templateUrl: '/app/Views/create.event.component.html',
            styleUrls: ['app/Styles/account.css']
        }), 
        __metadata('design:paramtypes', [account_service_1.AccountService, event_service_1.EventService, router_1.Router])
    ], CreateEventComponent);
    return CreateEventComponent;
}());
exports.CreateEventComponent = CreateEventComponent;
//# sourceMappingURL=create.event.component.js.map