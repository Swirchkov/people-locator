/**
 * Created by Vladimir on 29.10.2016.
 */

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { MainComponent } from './main.component';

import { LoginComponent } from "./login.component";
import { RegisterComponent } from "./register.component";

import { ScheduleComponent } from "./schedule.component";

import { CreateEventComponent } from "./create.event.component";
import { UpdateEventComponent } from "./update.event.component";
import { DeleteEventComponent } from "./delete.event.component";

import { SearchUserComponent } from "./search.user.component"; 

const routes : Routes = [
  {
    path : '',
    redirectTo: '/main',
    pathMatch: 'full'
  },
  {
    path: 'main',
    component: MainComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path : "schedule",
    component : ScheduleComponent
  },
  {
    path: "createEvent",
    component: CreateEventComponent
  },
  {
    path: 'update-event',
    component: UpdateEventComponent
  },
  {
    path: 'delete-event',
    component: DeleteEventComponent
  },
  {
    path: 'userSearch',
    component: SearchUserComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
  
}
