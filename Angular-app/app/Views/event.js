"use strict";
var Event = (function () {
    function Event(id, description, start, end, place) {
        if (id === void 0) { id = 0; }
        if (description === void 0) { description = null; }
        if (start === void 0) { start = null; }
        if (end === void 0) { end = null; }
        if (place === void 0) { place = null; }
        this.id = id;
        this.description = description;
        this.start = start;
        this.end = end;
        this.place = place;
    }
    Object.defineProperty(Event.prototype, "Id", {
        get: function () { return this.id; },
        set: function (value) { this.id = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "Description", {
        get: function () { return this.description; },
        set: function (value) { this.description = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "Start", {
        get: function () { return this.start; },
        set: function (value) { this.start = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "End", {
        get: function () { return this.end; },
        set: function (value) { this.end = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Event.prototype, "Place", {
        get: function () { return this.place; },
        set: function (value) { this.place = value; },
        enumerable: true,
        configurable: true
    });
    return Event;
}());
exports.Event = Event;
//# sourceMappingURL=event.js.map