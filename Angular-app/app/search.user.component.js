"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//services
var account_service_1 = require("./Services/account.service");
var friend_service_1 = require("./Services/friend.service");
var request_service_1 = require("./Services/request.service");
// library
var Session_1 = require("./Library/Session");
var SearchedUser_1 = require("./Models/SearchedUser");
var Friend_1 = require("./Models/Friend");
var MeetingRequest_1 = require("./Models/MeetingRequest");
var SearchUserComponent = (function () {
    function SearchUserComponent(userService, router, friendService, requestService) {
        this.userService = userService;
        this.router = router;
        this.friendService = friendService;
        this.requestService = requestService;
        // friends cud part
        this.addingFriend = null;
        this.answeringFriend = null;
        this.deletingFriend = null;
        // relation statuses
        this.NO_RELATION = SearchedUser_1.UserRelation.NoRelation;
        this.SENT_REQUEST = SearchedUser_1.UserRelation.SentRequest;
        this.RECEIVED_REQUEST = SearchedUser_1.UserRelation.ReceivedRequest;
        this.FRIENDS = SearchedUser_1.UserRelation.Friends;
        // page statuses ( search users || display friends || meeting requests )
        this.SEARCH_USERS_MODE = 0;
        this.FRIENDS_MODE = 1;
        this.REQUEST_MODE = 2;
        // default status user search
        this.mode = 0;
        // friends part 
        this.sendedRequestUsers = null;
        this.receivedRequestUsers = null;
        this.friends = null;
        // meeting request part
        this.meetingMessage = null;
        this.meetingFriend = null;
        this.answeringMeetingUser = null;
        this.answeringMeeting = null;
        this.answeringMessage = null;
        // meeting requests
        this.sendedRequests = [];
        this.receivedRequests = [];
    }
    SearchUserComponent.prototype.ngOnInit = function () {
        var type = sessionStorage.getItem('page-type');
        this.mode = this.SEARCH_USERS_MODE;
        if (type == 'FRIENDS') {
            this.mode = this.FRIENDS_MODE;
        }
        else if (type == "REQUESTS") {
            this.mode = this.REQUEST_MODE;
        }
        this.query = localStorage.getItem('user-query');
        this.AuthenticatedUser = Session_1.Session.AuthenticatedUser;
        this.filterUsers();
        this.loadRequests();
    };
    SearchUserComponent.prototype.loadRequests = function () {
        var _this = this;
        this.requestService.GetReceivedRequests(this.AuthenticatedUser.Id).then(function (requests) {
            console.log(requests);
            _this.receivedRequests = requests;
        });
        this.requestService.GetSentRequests(this.AuthenticatedUser.Id).then(function (requests) {
            //console.log(requests);
            _this.sendedRequests = requests;
        });
    };
    SearchUserComponent.prototype.filterUsers = function () {
        var userId = this.AuthenticatedUser != null ? this.AuthenticatedUser.Id : 0;
        if (this.mode == this.SEARCH_USERS_MODE) {
            this.loadUsersByLogin(userId, this.query);
        }
        else if (this.mode == this.FRIENDS_MODE) {
            this.loadFriends(userId);
        }
    };
    SearchUserComponent.prototype.loadUsersByLogin = function (userId, query) {
        var _this = this;
        this.userService.SearchByLogin(userId, query).then(function (users) {
            _this.users = users;
        });
    };
    SearchUserComponent.prototype.loadFriends = function (userId) {
        var _this = this;
        this.friendService.GetUserFriends(userId).then(function (res) {
            _this.friends = res.filter(function (user) { return user.Relation == SearchedUser_1.UserRelation.Friends; });
            _this.sendedRequestUsers = res.filter(function (user) { return user.Relation == SearchedUser_1.UserRelation.SentRequest; });
            _this.receivedRequestUsers = res.filter(function (user) { return user.Relation == SearchedUser_1.UserRelation.ReceivedRequest; });
        });
    };
    SearchUserComponent.prototype.isInReceived = function (userId) {
        return this.receivedRequests.find(function (req) { return req.Requester.Id == userId; }) != null;
    };
    SearchUserComponent.prototype.isInSended = function (userId) {
        return this.sendedRequests.find(function (req) { return req.Receiver.Id == userId; }) != null;
    };
    SearchUserComponent.prototype.isInAccepted = function (userId) {
        return this.sendedRequests.findIndex(function (req) { return req.Receiver.Id == userId && req.AnswerText != null; }) != -1 ||
            this.receivedRequests.findIndex(function (req) { return req.Requester.Id == userId && req.AnswerText != null; }) != -1;
    };
    SearchUserComponent.prototype.searchQuery = function () {
        this.filterUsers();
    };
    SearchUserComponent.prototype.switchMode = function (mode) {
        this.mode = mode;
        this.filterUsers();
        if (mode == this.SEARCH_USERS_MODE) {
            sessionStorage.removeItem('page-type');
        }
        else if (mode == this.FRIENDS_MODE) {
            sessionStorage.setItem('page-type', 'FRIENDS');
        }
        else if (mode == this.REQUEST_MODE) {
            sessionStorage.setItem('page-type', 'REQUESTS');
        }
    };
    SearchUserComponent.prototype.moveToSchedule = function (user) {
        console.log(user);
        sessionStorage.setItem('schedule-user', JSON.stringify(user));
        this.router.navigate(['/schedule']);
    };
    SearchUserComponent.prototype.addFriend = function () {
        var _this = this;
        if (this.AuthenticatedUser == null || this.AuthenticatedUser.Id == this.addingFriend.User.Id) {
            return;
        }
        var friend = new Friend_1.Friend();
        friend.User1Id = this.AuthenticatedUser.Id;
        friend.User2Id = this.addingFriend.User.Id;
        friend.IsAccepted = false;
        this.friendService.CreateFriend(friend).then(function (res) {
            if (!res.IsSuccess) {
            }
            else {
                _this.addingFriend.Relation = SearchedUser_1.UserRelation.SentRequest;
                _this.sendedRequestUsers.push(_this.addingFriend);
                _this.addingFriend = null;
            }
        });
    };
    SearchUserComponent.prototype.answerFriend = function () {
        var _this = this;
        if (this.AuthenticatedUser == null || this.AuthenticatedUser.Id == this.answeringFriend.User.Id) {
            return;
        }
        var friend = new Friend_1.Friend();
        friend.User1Id = this.AuthenticatedUser.Id;
        friend.User2Id = this.answeringFriend.User.Id;
        friend.IsAccepted = true;
        this.friendService.UpdateFriend(friend).then(function (res) {
            if (res.IsSuccess) {
                _this.answeringFriend.Relation = SearchedUser_1.UserRelation.Friends;
                var ansFriendIndex = _this.receivedRequestUsers.findIndex(function (fr) { return fr.User.Id == _this.answeringFriend.User.Id; });
                _this.receivedRequestUsers.splice(ansFriendIndex, 1);
                _this.friends.push(_this.answeringFriend);
                _this.answeringFriend = null;
            }
            else {
            }
        });
    };
    SearchUserComponent.prototype.deleteFriend = function () {
        var _this = this;
        if (this.AuthenticatedUser == null || this.AuthenticatedUser.Id == this.deletingFriend.User.Id) {
            return;
        }
        this.friendService.DeleteFriend(this.AuthenticatedUser.Id, this.deletingFriend.User.Id).then(function (res) {
            if (res.IsSuccess) {
                _this.deletingFriend.Relation = SearchedUser_1.UserRelation.NoRelation;
                var friendIndex = _this.friends.findIndex(function (fr) { return fr.User.Id == _this.deletingFriend.User.Id; });
                _this.friends.splice(friendIndex, 1);
                _this.deletingFriend = null;
            }
            else {
            }
        });
    };
    SearchUserComponent.prototype.meetFriend = function () {
        var _this = this;
        var request = new MeetingRequest_1.MeetingRequest(0, this.AuthenticatedUser, this.meetingFriend.User, this.meetingMessage, null, null);
        this.requestService.CreateRequest(request).then(function (res) {
            if (res.IsSuccess) {
                _this.meetingFriend = null;
                _this.loadRequests();
            }
        });
    };
    SearchUserComponent.prototype.answerMeet = function (user) {
        var request = this.receivedRequests.find(function (req) { return req.Requester.Id == user.User.Id; });
        console.log(request);
        this.answeringMeeting = request;
        this.answeringMeetingUser = user;
    };
    SearchUserComponent.prototype.answerMeetWrapper = function (user, relation) {
        this.answerMeet(new SearchedUser_1.SearchedUser(user, relation));
    };
    SearchUserComponent.prototype.sendMeetAnswer = function () {
        var _this = this;
        var request = this.answeringMeeting;
        request.AnswerText = this.answeringMessage != null ? this.answeringMessage : "";
        this.requestService.UpdateRequest(request).then(function (res) {
            if (res.IsSuccess) {
                _this.answeringMeetingUser = null;
                _this.loadRequests();
            }
        });
    };
    SearchUserComponent = __decorate([
        core_1.Component({
            selector: 'searchUser',
            templateUrl: '/app/Views/search.user.component.html',
            styleUrls: ['app/Styles/search.user.component.css']
        }), 
        __metadata('design:paramtypes', [account_service_1.AccountService, router_1.Router, friend_service_1.FriendService, request_service_1.RequestService])
    ], SearchUserComponent);
    return SearchUserComponent;
}());
exports.SearchUserComponent = SearchUserComponent;
//# sourceMappingURL=search.user.component.js.map