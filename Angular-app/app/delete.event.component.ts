import { Component } from "@angular/core";
import { Router } from "@angular/router";

//Services
import { EventService } from "./Services/event.service";

// Models
import { Event } from "./Models/Event";
import { User } from "./Models/User";

//lib
import { Session } from "./Library/Session";

@Component({
    selector: 'delete-event',
    templateUrl: "/app/Views/delete.event.component.html",
    styleUrls: ['app/Styles/schedule.component.css']
})
export class DeleteEventComponent {
    private event: Event;
    private user: User;

    constructor(private router : Router, private eventService: EventService) {}

    ngOnInit() {
        this.user = Session.AuthenticatedUser;

        if (this.user == null) {
            this.router.navigate(['/login']);
            return;
        }

        let jsonEvent = JSON.parse(localStorage.getItem("delete-Event"));

        if (jsonEvent == null) {
            this.router.navigate(['/schedule']);
            return;
        }
        
        this.event = new Event(jsonEvent.id, jsonEvent.description, new Date(jsonEvent.start), 
                    new Date(jsonEvent.end), jsonEvent.place, jsonEvent.userId);
    }

    delete() {
        this.eventService.DeleteEvent(this.event.Id).then(res => {
            if (res.IsSuccess) {
                this.router.navigate([ '/schedule' ]);
            }
            else {
                console.warn(res.Message);
            }
        });
    }

    cancel() {
        this.router.navigate([ '/schedule' ]);
    }
}