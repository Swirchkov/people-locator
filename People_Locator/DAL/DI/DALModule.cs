﻿using Autofac;
using DAL.Repositories;
using DAL.Models;

namespace DAL.DI
{
    public class DALModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));
            builder.RegisterType<UserRespository>().As<IRepository<User>>();

            base.Load(builder);
        }
    }
}