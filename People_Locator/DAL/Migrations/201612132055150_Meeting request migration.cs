namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Meetingrequestmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MeetingRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequesterId = c.Int(nullable: false),
                        ReceiverId = c.Int(nullable: false),
                        RequestText = c.String(),
                        AnswerText = c.String(),
                        FinishDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ReceiverId)
                .ForeignKey("dbo.Users", t => t.RequesterId)
                .Index(t => t.RequesterId)
                .Index(t => t.ReceiverId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MeetingRequests", "RequesterId", "dbo.Users");
            DropForeignKey("dbo.MeetingRequests", "ReceiverId", "dbo.Users");
            DropIndex("dbo.MeetingRequests", new[] { "ReceiverId" });
            DropIndex("dbo.MeetingRequests", new[] { "RequesterId" });
            DropTable("dbo.MeetingRequests");
        }
    }
}
