namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class friendmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        User1Id = c.Int(nullable: false),
                        User2Id = c.Int(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.User1Id, t.User2Id })
                .ForeignKey("dbo.Users", t => t.User1Id)
                .ForeignKey("dbo.Users", t => t.User2Id)
                .Index(t => t.User1Id)
                .Index(t => t.User2Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Friends", "User2Id", "dbo.Users");
            DropForeignKey("dbo.Friends", "User1Id", "dbo.Users");
            DropIndex("dbo.Friends", new[] { "User2Id" });
            DropIndex("dbo.Friends", new[] { "User1Id" });
            DropTable("dbo.Friends");
        }
    }
}
