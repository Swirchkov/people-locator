﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DAL.Models;

namespace DAL
{
    
    class PeopleContext : DbContext
    {
        public PeopleContext() : base("name=Default")
        {
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Friend> Friends { get; set; }
        public DbSet<MeetingRequest> Requests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Keys
            modelBuilder.Entity<User>().HasKey(u => u.Id);
            modelBuilder.Entity<Event>().HasKey(e => e.EventId);
            modelBuilder.Entity<Friend>().HasKey(f => new { f.User1Id, f.User2Id });
            modelBuilder.Entity<MeetingRequest>().HasKey(req => req.Id);

            // Foreign keys
            modelBuilder.Entity<User>().HasMany(u => u.Events).WithRequired(e => e.User).HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Friend>().HasRequired(f => f.User1).WithMany(u => u.Friends_1).HasForeignKey(f => f.User1Id).WillCascadeOnDelete(false);
            modelBuilder.Entity<Friend>().HasRequired(f => f.User2).WithMany(u => u.Friends_2).HasForeignKey(f => f.User2Id).WillCascadeOnDelete(false);

            modelBuilder.Entity<MeetingRequest>().HasRequired(req => req.Receiver).WithMany(u => u.ReceivedRequests).HasForeignKey(req => req.ReceiverId).WillCascadeOnDelete(false);
            modelBuilder.Entity<MeetingRequest>().HasRequired(req => req.Requester).WithMany(u => u.SentRequests).HasForeignKey(req => req.RequesterId).WillCascadeOnDelete(false);

            // column mapping fixing
            modelBuilder.Entity<Event>().Property<DateTime>(e => e.Start).HasColumnType("datetime2");
            modelBuilder.Entity<Event>().Property<DateTime>(e => e.End).HasColumnType("datetime2");
            modelBuilder.Entity<MeetingRequest>().Property<DateTime>(req => req.FinishDate).HasColumnType("datetime2");

            base.OnModelCreating(modelBuilder);
        }
    }
}
