﻿
namespace DAL.Models
{
    public class Friend : AbstractModel
    {
        public int User1Id { get; set; }
        public int User2Id { get; set; }
        public bool IsAccepted { get; set; }

        public virtual User User1 { get; set; }
        public virtual User User2 { get; set; }

        public override object[] Key
        {
            get
            {
                return new object[] { User1Id, User2Id };
            }
        }

        protected override void copyValuesFrom(AbstractModel model)
        {
            Friend friend = (Friend)model;

            this.User1Id = friend.User1Id;
            this.User2Id = friend.User2Id;
            this.IsAccepted = friend.IsAccepted;

        }
    }
}
