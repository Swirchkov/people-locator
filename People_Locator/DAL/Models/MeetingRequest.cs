﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class MeetingRequest : AbstractModel
    {
        public int Id { get; set; }
        public int RequesterId { get; set; }
        public int ReceiverId { get; set; }
        public string RequestText { get; set; }
        public string AnswerText { get; set; }
        public DateTime FinishDate { get; set; }

        public virtual User Requester { get; set; }
        public virtual User Receiver { get; set; }

        public override object[] Key
        {
            get
            {
                return new object[] { Id };
            }
        }

        protected override void copyValuesFrom(AbstractModel model)
        {
            MeetingRequest req = (MeetingRequest)model;

            this.ReceiverId = req.ReceiverId;
            this.RequesterId = req.RequesterId;
            this.AnswerText = req.AnswerText;
            this.FinishDate = req.FinishDate;
            this.RequestText = req.RequestText;

        }
    }
}
