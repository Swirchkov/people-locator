﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    abstract public class AbstractModel
    {
        // required for correct update operation using EF
        public void CopyFrom(AbstractModel model)
        {
            this.copyValuesFrom(model);
        }

        protected abstract void copyValuesFrom(AbstractModel model);

        // required to provide possibility to find object by key using EF
        public abstract object[] Key { get; }
    }
}
