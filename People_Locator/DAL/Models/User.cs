﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class User : AbstractModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Image { get; set; }
        public string Company { get; set; }
        public string Password { get; set; }

        public string Email { get; set; }
        public string Facebook { get; set; }
        public string VK { get; set; }

        public virtual ICollection<Event> Events { get; set; }

        internal virtual ICollection<Friend> Friends_1 { get; set; }
        internal virtual ICollection<Friend> Friends_2 { get; set; }

        public IEnumerable<Friend> Friends
        {
            get
            {
                return Friends_1.Concat(Friends_2).Distinct();
            }
        }

        public virtual ICollection<MeetingRequest> SentRequests { get; set; }
        public virtual ICollection<MeetingRequest> ReceivedRequests { get; set; }

        public override object[] Key
        {
            get
            {
                return new object[] { this.Id };
            }
        }

        protected override void copyValuesFrom(AbstractModel model)
        {
            User other = (User)model;

            this.Company = other.Company;
            this.Email = other.Email;
            this.Facebook = other.Facebook;
            this.Image = other.Image;
            this.Login = other.Login;
            this.Password = other.Password;
            this.VK = other.VK;

        }
    }
}
