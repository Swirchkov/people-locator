﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Event : AbstractModel
    {
        public int EventId { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Place { get; set; }
        public EventAvailability AvailableTo { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }

        public override object[] Key
        {
            get
            {
                return new object[] { this.EventId };
            }
        }

        protected override void copyValuesFrom(AbstractModel model)
        {
            Event other = (Event)model;

            this.Start = other.Start;
            this.AvailableTo = other.AvailableTo;
            this.End = other.End;
            this.Place = other.Place;
            this.UserId = other.UserId;
            this.Description = other.Description;
        }
    }

    public enum EventAvailability
    {
        Private,
        Friends,
        Public
    }
}
