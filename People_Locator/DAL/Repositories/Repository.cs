﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    class Repository<T> : IRepository<T> where T : AbstractModel
    {
        protected PeopleContext db = new PeopleContext();

        public Repository() {  }

        public virtual void Add(T item)
        {
            db.Set<T>().Add(item);
        }

        public virtual bool Delete(params int[] id)
        {
            T item = null;
            if (id.Length > 1)
            {
                item = db.Set<T>().Find(id.Select(i => i as object).ToArray());
            }
            else
            {
                item = db.Set<T>().Find(id[0]);
            }

            if (item != null)
            {
                db.Set<T>().Remove(item);
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return db.Set<T>().Where(predicate);
        }

        public virtual void Update(T item)
        {
            object[] keys = item.Key;
            T current = db.Set<T>().Find(keys);

            current.CopyFrom(item);

            db.Entry(current).State = EntityState.Modified;
        }

        public void SaveChanges() => this.db.SaveChanges();

    }
}
