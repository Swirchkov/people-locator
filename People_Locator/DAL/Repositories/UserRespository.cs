﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    class UserRespository : Repository<User>
    {
        public override bool Delete(params int[] key)
        {
            int id = key[0];

            db.Friends.RemoveRange(db.Friends.Where(f => f.User1Id == id || f.User2Id == id));
            db.Requests.RemoveRange(db.Requests.Where(req => req.ReceiverId == id || req.RequesterId == id));

            return base.Delete(key);
        }
    }
}
