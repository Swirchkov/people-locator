﻿using AutoMapper;
using DAL.Api.Models;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Api.Util
{
    internal static class MapperConfig
    {
        internal static void Config()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Event, EventViewModel>();
                cfg.CreateMap<User, UserViewModel>();
                cfg.CreateMap<Friend, FriendViewModel>();
                cfg.CreateMap<MeetingRequest, MeetingRequestViewModel>();
            });
        }

    }
}
