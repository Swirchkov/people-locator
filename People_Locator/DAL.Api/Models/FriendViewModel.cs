﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Api.Models
{
    public class FriendViewModel
    {
        public int User1Id { get; set; }
        public int User2Id { get; set; }
        public bool IsAccepted { get; set; }
    }
}
