﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Api.Models
{
    public class EventViewModel
    {
        public int EventId { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Place { get; set; }
        public EventAvailability AvailableTo { get; set; }
        public int UserId { get; set; }

    }
}
