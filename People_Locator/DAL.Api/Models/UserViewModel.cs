﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Api.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Image { get; set; }
        public string Company { get; set; }
        public string Password { get; set; }

        public string Email { get; set; }
        public string Facebook { get; set; }
        public string VK { get; set; }
    }
}
