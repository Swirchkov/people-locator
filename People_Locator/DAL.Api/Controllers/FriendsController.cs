﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Repositories;
using AutoMapper;
using DAL.Api.Models;

namespace DAL.Api.Controllers
{
    public class FriendsController : ApiController
    {
        private IRepository<Friend> repo = (IRepository<Friend>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IRepository<Friend>));

        public IEnumerable<FriendViewModel> GetAll()
        {
            return repo.Find(f => true).Select(f => Mapper.Map<FriendViewModel>(f));
        }

        public FriendViewModel Get(int id1, int id2)
        {
            return Mapper.Map<FriendViewModel>(repo.Find(f => f.User1Id == id1 && f.User2Id == id2).FirstOrDefault());
        }

        [HttpPost]
        public bool Create(Friend friend)
        {
            if (friend == null) { return false; }

            repo.Add(friend);
            repo.SaveChanges();

            return true;
        }

        [HttpPut]
        public bool Update(Friend friend)
        {
            if (friend == null) { return false; }

            repo.Update(friend);
            repo.SaveChanges();

            return true;
        }

        [HttpDelete]
        public bool Delete(int id1, int id2)
        {
            if (id1 <= 0 || id2 <= 0) { return false; }

            repo.Delete(id1, id2);
            repo.SaveChanges();

            return true;
        }
    }
}
