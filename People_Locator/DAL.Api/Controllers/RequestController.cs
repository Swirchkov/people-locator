﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Models;
using DAL.Repositories;
using System.Web.Mvc;
using DAL.Api.Models;
using AutoMapper;

namespace DAL.Api.Controllers
{
    public class RequestController : ApiController
    {
        private IRepository<MeetingRequest> repo = DependencyResolver.Current.GetService<IRepository<MeetingRequest>>();

        [System.Web.Http.HttpGet]
        public IEnumerable<MeetingRequestViewModel> GetAll()
        {
            return repo.Find(req => true).Select(Mapper.Map<MeetingRequestViewModel>);
        }

        [System.Web.Http.HttpGet]
        public MeetingRequestViewModel GetById(int id)
        {
            return Mapper.Map<MeetingRequestViewModel>(repo.Find(req => req.Id == id).FirstOrDefault());
        } 
 
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/request/received")]
        public IEnumerable<MeetingRequestViewModel> GetReceivedRequests(int id)
        {
            return repo.Find(req => req.ReceiverId == id).Select(Mapper.Map<MeetingRequestViewModel>);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/request/sended")]
        public IEnumerable<MeetingRequestViewModel> GetSentRequests(int id)
        {
            return repo.Find(req => req.RequesterId == id).Select(Mapper.Map<MeetingRequestViewModel>);
        }

        [System.Web.Http.HttpPost]
        public bool CreateRequest(MeetingRequest req)
        {
            if (req == null) { return false; }

            repo.Add(req);
            repo.SaveChanges();

            return true;
        }

        [System.Web.Http.HttpPut]
        public bool UpdateRequest(MeetingRequest req)
        {
            if (req == null) { return false; }

            repo.Update(req);
            repo.SaveChanges();

            return true;
        }

        [System.Web.Http.HttpDelete]
        public bool DeleteRequest(int id)
        {
            if (id <= 0) { return false; }

            repo.Delete(id);
            repo.SaveChanges();

            return true;
        }
    }
}
