﻿using DAL.Api.Models;
using DAL.Models;
using DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;

namespace DAL.Api.Controllers
{
    public class EventController : ApiController
    {
        private IRepository<Event> repo = (IRepository<Event>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IRepository<Event>));

        [HttpGet]
        public IEnumerable<EventViewModel> GetAll()
        {
            return repo.Find(e => true).Select(ev => Mapper.Map<EventViewModel>(ev));
        }

        [HttpGet]
        public EventViewModel Get(int id)
        {
            return Mapper.Map<EventViewModel>(repo.Find(e => e.EventId == id).FirstOrDefault());
        }

        [HttpPost]
        public bool CreateEvent(Event e)
        {
            if (e == null) { return false; }

            repo.Add(e);
            repo.SaveChanges();

            return true;
        }

        [HttpPut]
        public bool UpdateEvent(Event e)
        {
            if (e == null) { return false; }

            repo.Update(e);
            repo.SaveChanges();

            return true;
        }

        [HttpDelete]
        public bool DeleteItem(int id)
        {
            if (id <= 0) { return false; }

            repo.Delete(id);
            repo.SaveChanges();

            return true;
        }

    }
}
