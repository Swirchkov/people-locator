﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Models;
using DAL.Repositories;
using System.Web.Mvc;
using DAL.Api.Models;
using AutoMapper;

namespace DAL.Api.Controllers
{
    public class UserController : ApiController
    {
        private IRepository<User> repo = DependencyResolver.Current.GetService<IRepository<User>>();

        public UserViewModel GetUser(string login)
        {
            return Mapper.Map<UserViewModel>(repo.Find(user => user.Login == login).FirstOrDefault());
        }

        public UserViewModel GetUserById(int id)
        {
            return Mapper.Map<UserViewModel>(repo.Find(user => user.Id == id).FirstOrDefault());
        }

        public IEnumerable<UserViewModel> GetUsers()
        {
            return repo.Find(user => true).Select(user => Mapper.Map<UserViewModel>(user));
        }

        [System.Web.Http.HttpPost]
        public bool CreateUser(User user)
        {
            if (user == null) { return false; }

            // exception filter will catch exceptions
            repo.Add(user);
            repo.SaveChanges();

            return true;
        }

        [System.Web.Http.HttpPut]
        public bool UpdateUser(User user)
        {
            if (user == null) { return false; }

            repo.Update(user);
            repo.SaveChanges();

            return true;
        }

        [System.Web.Http.HttpDelete]
        public bool DeleteUser(int id)
        {
            bool result =repo.Delete(id);

            if (result)
            {
                repo.SaveChanges();
            }

            return result;
        }
    }
}
