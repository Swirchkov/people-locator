﻿using Loader.DTO;
using BLL.Services.Entity;
using BLL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BLL.Api.Controllers
{
    public class EventController : ApiController
    {
        private IEventService service = (IEventService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IEventService));

        [HttpGet]
        public IEnumerable<EventDTO> GetAll()
        {
            return service.Find(ev => true);
        }

        [HttpGet]
        public EventDTO GetById(int id)
        {
            return service.Find(ev => ev.EventId == id).FirstOrDefault();
        }

        [HttpGet]
        [Route("api/event/eventsOfDay")]
        public IEnumerable<EventDTO> GetEventsOfDay(int year, int month, int day, int userId, int scheduleUserId)
        {
            DateTime dt = new DateTime(year, month, day);

            IUserService userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));

            UserDTO requester = userService.Find(u => u.Id == userId).FirstOrDefault();
            UserDTO requested = userService.Find(u => u.Id == scheduleUserId).FirstOrDefault();

            if (requested == null)
            {
                return null;
            }

            return service.GetEventsOfDay(dt, requester, requested);
        }

        [HttpPost]
        public CUDResponseView CreateEvent(EventDTO ev)
        {
            try
            {
                service.Create(ev);
            }
            catch (ValidationException e)
            {
                return CUDResponseView.BuildErrorResponse(e.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPut]
        public CUDResponseView UpdateEvent(EventDTO ev)
        {
            try
            {
                service.Update(ev);
            }
            catch (ValidationException e)
            {
                return CUDResponseView.BuildErrorResponse(e.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpDelete]
        public CUDResponseView DeleteEvent(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (ValidationException e)
            {
                return CUDResponseView.BuildErrorResponse(e.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }
    }
}
