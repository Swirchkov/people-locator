﻿using System.Collections.Generic;
using System.Web.Http;
using Loader.DTO;
using BLL.Services.Entity;
using System.Web.Mvc;
using BLL.Validation;
using System.Linq;

namespace BLL.Api.Controllers
{
    public class UserController : ApiController
    {
        private IUserService service = DependencyResolver.Current.GetService<IUserService>();

        private UserRelation getRelation(int user1Id, UserDTO user2)
        {
            IFriendService service = DependencyResolver.Current.GetService<IFriendService>();
            FriendDTO friend = service.Find(f => (f.User1Id == user1Id && f.User2Id == user2.Id) || 
            (f.User1Id == user2.Id && f.User2Id == user1Id)).FirstOrDefault();

            if (friend == null)
            {
                return UserRelation.NoRelation;
            }
            else if (friend.User1Id == user1Id && !friend.IsAccepted)
            {
                return UserRelation.SentRequest;
            }
            else if (friend.User2Id == user1Id && !friend.IsAccepted)
            {
                return UserRelation.ReceivedRequest;
            }
            else
            {
                return UserRelation.Friend;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/user/search")]
        public IEnumerable<SearchedUserDTO> SearchUsers(int userId, string query)
        {
            IEnumerable<UserDTO> users = service.Find(u => u.Login.Contains(query));
            UserDTO user = service.Find(u => u.Id == userId).FirstOrDefault();

            return users.Select(u => new SearchedUserDTO() { User = u, Relation = getRelation(userId, u) });
        }

        private CUDResponseView registerUser(UserDTO user)
        {
            try
            {
                service.Register(user);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [System.Web.Http.HttpGet]
        public UserDTO GetById(int id)
        {
            return service.Find(u => u.Id == id).FirstOrDefault();
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/user/login")]
        public UserDTO Login(string login, string password) => service.Login(login, password);

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/user/register")]
        public CUDResponseView Register(UserDTO user)
        {
            return this.registerUser(user);
        }

        [System.Web.Http.HttpPost]
        public CUDResponseView CreateUser(UserDTO user)
        {
            return this.registerUser(user);
        }

        public IEnumerable<UserDTO> GetAll() => service.Find(user => true);

        [System.Web.Http.HttpPut]
        public CUDResponseView UpdateItem(UserDTO user)
        {
            try
            {
                service.Update(user);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [System.Web.Http.HttpDelete]
        public CUDResponseView DeleteItem(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }
    }
}
