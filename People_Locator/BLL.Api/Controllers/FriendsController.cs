﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Loader;
using Loader.DTO;
using BLL.Services.Entity;
using BLL.Validation;

namespace BLL.Api.Controllers
{
    public class FriendsController : ApiController
    {
        private IFriendService friendService = (IFriendService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IFriendService));

        [Route("api/friends/getUserFriends")]
        public IEnumerable<SearchedUserDTO> GetUserFriends(int id)
        {
            return friendService.GetUserFriends(id);
        }

        [HttpPost]
        public CUDResponseView CreateFriend(FriendDTO friend)
        {
            try
            {
                friendService.Create(friend);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPut]
        public CUDResponseView UpdateFriend(FriendDTO friend)
        {
            try
            {
                friendService.Update(friend);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpDelete]
        public CUDResponseView DeleteFriend(int id1, int id2)
        {
            try
            {
                friendService.Delete(id1, id2);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

    }
}
