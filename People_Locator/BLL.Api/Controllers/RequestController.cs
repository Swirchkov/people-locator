﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL.Services.Entity;
using System.Web.Mvc;
using Loader.DTO;
using BLL.Validation;
using Loader.CustomRequests;

namespace BLL.Api.Controllers
{
    public class RequestController : ApiController
    {
        private IRequestService service = DependencyResolver.Current.GetService<IRequestService>();
        private readonly string DalPath = "http://localhost:4241/api/";

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/request/received")]
        public IEnumerable<MeetingRequestDTO> GetReceivedRequests(int userId)
        {
            ICustomRequest custom = DependencyResolver.Current.GetService<ICustomRequest>();

            return custom.Load<IEnumerable<MeetingRequestDTO>>(HttpMethod.Get, this.DalPath + "request/received", new Dictionary<string, string>()
            {
                { "id", userId.ToString() } 
            });
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/request/sended")]
        public IEnumerable<MeetingRequestDTO> GetSentRequests(int userId)
        {
            ICustomRequest custom = DependencyResolver.Current.GetService<ICustomRequest>();

            return custom.Load<IEnumerable<MeetingRequestDTO>>(HttpMethod.Get, this.DalPath + "request/sended", new Dictionary<string, string>()
            {
                { "id", userId.ToString() }
            });
        }

        [System.Web.Http.HttpPost]
        public CUDResponseView CreateRequest(MeetingRequestDTO req)
        {
            try
            {
                service.Create(req);
            }
            catch (ValidationException e)
            {
                return CUDResponseView.BuildErrorResponse(e.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [System.Web.Http.HttpPut]
        public CUDResponseView UpdateRequest(MeetingRequestDTO req)
        {
            try
            {
                service.Update(req);
            }
            catch (ValidationException e)
            {
                return CUDResponseView.BuildErrorResponse(e.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [System.Web.Http.HttpDelete]
        public CUDResponseView DeleteRequest(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (ValidationException e)
            {
                return CUDResponseView.BuildErrorResponse(e.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }
    }
}
