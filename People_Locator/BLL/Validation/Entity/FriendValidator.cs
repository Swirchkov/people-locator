﻿using Loader;
using Loader.DTO;
using BLL.DI;
using Autofac;
using System.Linq;

namespace BLL.Validation.Entity
{
    class FriendValidator : Validator<FriendDTO>
    {
        private ILoader<FriendDTO> loader = AutofacResolver.Container.Resolve<ILoader<FriendDTO>>();

        private bool loadUser(int id)
        {
            ILoader<UserDTO> userLoader = AutofacResolver.Container.Resolve<ILoader<UserDTO>>();
            return userLoader.LoadById(id) != null;
        }

        protected override bool existsItem(params int[] id)
        {
            return loader.LoadById(id) != null;
        }

        protected override bool existsItem(FriendDTO item)
        {
            return this.existsItem(item.User1Id, item.User2Id);
        }

        protected override bool validateItemProperties(FriendDTO item)
        {
            if (item.User1Id <= 0 || item.User2Id <= 0)
            {
                return false;
            }

            return loadUser(item.User1Id) && loadUser(item.User2Id);
        }

    }
}
