﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loader.DTO;
using Loader;
using BLL.DI;
using Autofac;

namespace BLL.Validation.Entity
{
    internal class RequestValidator : Validator<MeetingRequestDTO>
    {
        private ILoader<MeetingRequestDTO> loader = AutofacResolver.Container.Resolve<ILoader<MeetingRequestDTO>>();

        private bool existsUser(int id)
        {
            ILoader<UserDTO> userLoader = AutofacResolver.Container.Resolve<ILoader<UserDTO>>();
            return userLoader.LoadById(id) != null;
        }

        // called before delete
        protected override bool existsItem(params int[] id)
        {
            return loader.LoadById(id) != null;
        }

        // called before create & update
        protected override bool existsItem(MeetingRequestDTO item)
        {
            if (loader.LoadById(item.Id) != null)
            {
                return true;
            }
            else if (loader.LoadAll().Where(req => req.ReceiverId == item.ReceiverId && req.RequesterId == item.RequesterId && req.FinishDate == item.FinishDate).Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool validateItemProperties(MeetingRequestDTO item)
        {
            if (item.ReceiverId <= 0 || item.RequesterId <= 0)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(item.RequestText))
            {
                return false;
            }

            if (!existsUser(item.RequesterId) || !existsUser(item.ReceiverId))
            {
                return false;
            }

            return true;
        }
    }
}
