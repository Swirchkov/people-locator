﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loader.DTO;
using BLL.DI;
using Loader;
using Autofac;

namespace BLL.Validation.Entity
{
    internal class EventValidator : Validator<EventDTO>
    {
        protected override bool existsItem(params int[] ids)
        {
            ILoader<EventDTO> loader = AutofacResolver.Container.Resolve<ILoader<EventDTO>>();
            return loader.LoadById(ids[0]) != null;
        }

        protected override bool existsItem(EventDTO item)
        {
            return existsItem(item.EventId);
        }

        protected override bool validateItemProperties(EventDTO item)
        {
            if (item.End == default(DateTime) || item.Start == default(DateTime))
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(item.Place) || String.IsNullOrWhiteSpace(item.Description))
            {
                return false;
            }

            if (item.UserId <= 0)
            {
                return false;
            }

            ILoader<UserDTO> userLoader = AutofacResolver.Container.Resolve<ILoader<UserDTO>>();

            if (userLoader.LoadById(item.UserId) == null)
            {
                return false;
            }

            return true;
        }
    }
}
