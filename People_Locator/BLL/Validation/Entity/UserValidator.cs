﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loader.DTO;
using Loader;
using BLL.DI;
using Autofac;
using System.Text.RegularExpressions;

namespace BLL.Validation.Entity
{
    internal class UserValidator : Validator<UserDTO>
    {
        protected override bool existsItem(params int[] ids)
        {
            ILoader<UserDTO> loader = AutofacResolver.Container.Resolve<ILoader<UserDTO>>();
            return loader.LoadById(ids[0]) != null;
        }
        protected override bool existsItem(UserDTO item)
        {
            return existsItem(item.Id);
        }

        protected override bool validateItemProperties(UserDTO item)
        {
            if (string.IsNullOrWhiteSpace(item.Login) || string.IsNullOrWhiteSpace(item.Company) || string.IsNullOrWhiteSpace(item.Email) || string.IsNullOrWhiteSpace(item.Image) || string.IsNullOrWhiteSpace(item.Password))
            {
                return false;
            }

            // check email
            Regex exp = new Regex(@"\w+@\w+\.\w+");

            if (!exp.IsMatch(item.Email))
            {
                return false;
            }

            return true;
        }
    }
}
