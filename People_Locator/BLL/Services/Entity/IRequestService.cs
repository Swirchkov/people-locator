﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loader.DTO;

namespace BLL.Services.Entity
{
    public interface IRequestService : IService<MeetingRequestDTO>
    {
        IEnumerable<MeetingRequestDTO> GetReceivedRequests(int userId);
        IEnumerable<MeetingRequestDTO> GetSentRequests(int userId);
    }
}
