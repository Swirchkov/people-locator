﻿using Loader;
using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DI;
using Autofac;

namespace BLL.Services.Entity
{
    class FriendService : BaseService<FriendDTO>, IFriendService
    {
        private UserRelation getRelation(FriendDTO friend, int user1Id)
        {
            if (friend == null)
            {
                return UserRelation.NoRelation;
            }
            else if (friend.User1Id == user1Id && !friend.IsAccepted)
            {
                return UserRelation.SentRequest;
            }
            else if (friend.User2Id == user1Id && !friend.IsAccepted)
            {
                return UserRelation.ReceivedRequest;
            }
            else
            {
                return UserRelation.Friend;
            }
        }

        public IEnumerable<SearchedUserDTO> GetUserFriends(int userId)
        {
            IEnumerable<FriendDTO> friends = loader.LoadAll();
            friends = friends.Where(f => f.User1Id == userId || f.User2Id == userId);

            ILoader<UserDTO> userLoader = AutofacResolver.Container.Resolve<ILoader<UserDTO>>();

            return friends.Select(f => new SearchedUserDTO(getRelation(f, userId), userLoader.LoadById(f.User1Id == userId ? f.User2Id : f.User1Id)));
        }

        public override void Update(FriendDTO item)
        {
            if (loader.LoadById(item.User1Id, item.User2Id) == null)
            {
                // swap values
                int temp = item.User1Id;
                item.User1Id = item.User2Id;
                item.User2Id = temp;
            }
            base.Update(item);
        }

        public override void Delete(params int[] ids)
        {
            if (loader.LoadById(ids[0], ids[1]) == null)
            {
                // swap values
                int temp = ids[0];
                ids[0] = ids[1];
                ids[1] = temp;
            }
            base.Delete(ids);
        }
    }
}
