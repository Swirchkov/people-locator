﻿using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Entity
{
    public interface IFriendService : IService<FriendDTO>
    {
        IEnumerable<SearchedUserDTO> GetUserFriends(int userId); 
    }
}
