﻿using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Entity
{
    internal class EventService : BaseService<EventDTO>, IEventService
    {
        public IEnumerable<EventDTO> GetEventsOfDay(DateTime date, UserDTO requester, UserDTO requested)
        {
            return loader.LoadAll().Where(ev => this.checkEvent(ev, date, requester, requested));
        }

        private bool checkEvent(EventDTO ev, DateTime dt, UserDTO requester, UserDTO requested)
        {
            if (ev.UserId != requested.Id) { return false; }
            if (!(matchDay(dt, ev.Start) || matchDay(dt, ev.End))) { return false; }

            // check if the current user can access this
            if (requester == null) { return ev.AvailableTo == EventAvailabilityDTO.Public; }
             
            if (ev.UserId == requester.Id) { return true; }
            if (ev.AvailableTo == EventAvailabilityDTO.Public) { return true; }

            return false;
        }

        private bool matchDay(DateTime obj1, DateTime obj2)
        {
            return obj1.Year == obj2.Year && obj1.DayOfYear == obj2.DayOfYear;
        } 
    }
}
