﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loader.DTO;

namespace BLL.Services.Entity
{
    class RequestService : BaseService<MeetingRequestDTO>, IRequestService
    {
        public IEnumerable<MeetingRequestDTO> GetReceivedRequests(int userId)
        {
            return loader.LoadAll().Where(req => req.ReceiverId == userId);
        }

        public IEnumerable<MeetingRequestDTO> GetSentRequests(int userId)
        {
            return loader.LoadAll().Where(req => req.RequesterId == userId);
        }
    }
}
