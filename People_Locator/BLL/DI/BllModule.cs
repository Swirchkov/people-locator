﻿using Autofac;
using BLL.Services;
using Loader.DTO;
using BLL.Services.Entity;

namespace BLL.DI
{
    public class BllModule : Module
    {
        public BllModule(string path)
        {
            AutofacResolver.ConfigPath = path;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(BaseService<>)).As(typeof(IService<>));

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<UserService>().As<IService<UserDTO>>();

            builder.RegisterType<EventService>().As<IEventService>();
            builder.RegisterType<EventService>().As<IService<EventDTO>>();

            builder.RegisterType<FriendService>().As<IService<FriendDTO>>();
            builder.RegisterType<FriendService>().As<IFriendService>();

            builder.RegisterType<RequestService>().As<IService<MeetingRequestDTO>>();
            builder.RegisterType<RequestService>().As<IRequestService>();

            base.Load(builder);
        }
    }
}
