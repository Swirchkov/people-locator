﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Loader.DI;
using BLL.Validation;
using Loader.DTO;
using BLL.Validation.Entity;

namespace BLL.DI
{
    static class AutofacResolver
    {
        private static IContainer container;

        public static string ConfigPath { get;set;}

        public static IContainer Container
        {
            get
            {
                if (container == null)
                    container = buildContainer();

                return container;
            }
        }

        private static IContainer buildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule(new LoaderModule(ConfigPath));

            builder.RegisterType<UserValidator>().As<IValidator<UserDTO>>();
            builder.RegisterType<EventValidator>().As<IValidator<EventDTO>>();
            builder.RegisterType<FriendValidator>().As <IValidator<FriendDTO>>();
            builder.RegisterType<RequestValidator>().As<IValidator<MeetingRequestDTO>>();

            return builder.Build();
        }
    }
}
