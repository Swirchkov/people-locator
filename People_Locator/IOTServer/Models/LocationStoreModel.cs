﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTServer.Models
{
    public class LocationStoreModel
    {
        public string StoreId { get; set; }
        public int UserId { get; set; }
        public string Location { get; set; }
        public DateTime Start { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
