﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IOTServer.Models;

namespace IOTServer.Controllers
{
    public class HomeController : Controller
    {
        private static List<LocationStoreModel> locations { get; set; } = new List<LocationStoreModel>();

        public ActionResult Index()
        {
            return View(locations);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public string SendRedirection(int userId, string location, string session)
        {
            LocationStoreModel model = locations.FirstOrDefault(loc => loc.StoreId == session);

            if (model == null)
            {
                model = new LocationStoreModel()
                {
                    UserId = userId,
                    Location = location,
                    StoreId = Guid.NewGuid().ToString(),
                    Start = DateTime.Now
                };
                locations.Add(model);
            }

            model.Duration = DateTime.Now - model.Start;

            return model.StoreId;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}