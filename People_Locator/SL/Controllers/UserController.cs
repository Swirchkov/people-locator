﻿using Loader;
using Loader.DTO;
using SL.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.IO;
using SL.Models;
using System.Collections.Specialized;
using Loader.CustomRequests;

namespace SL.Controllers
{
    public class UserController : ApiController
    {
        private ILoader<UserDTO> loader = (ILoader<UserDTO>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ILoader<UserDTO>));
        private readonly string BllPath = "http://localhost:17995/api";

        private string saveImage(HttpPostedFile file)
        {
            string path = HttpContext.Current.Server.MapPath("~/Images");
            string fileName = file.FileName;
            string ext = fileName.Split('.').Last();

            while (File.Exists(Path.Combine(path, fileName))) 
            {
                fileName = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12) + "." + ext;
            }

            file.SaveAs(Path.Combine(path, fileName));
            return "/Images/" + fileName;
        }

        [Route("api/user/login")]
        [HttpPost]
        public KeyValuePair<string, UserDTO>? Login(UserDTO user)
        {
            string login = user.Login;
            string password = user.Password;

            if (login == null || password == null)
            {
                return default(KeyValuePair<string, UserDTO>);
            }
            ICustomRequest custom = (ICustomRequest)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ICustomRequest));
            UserDTO dbUser = custom.Load<UserDTO>(HttpMethod.Get, this.BllPath + "/user/login", new Dictionary<string, string>() {
                { "Login", login },
                { "Password", password }
            });

            if (dbUser == null)
            {
                return null;
            }


            string token = TokenCollection.GenerateToken(user);

            return new KeyValuePair<string, UserDTO>(token, dbUser);
        }

        [Route("api/user/validatetoken")]
        [HttpGet]
        public UserDTO ValidateToken(string token)
        {
            return TokenCollection.GetByToken(token);
        }

        [HttpGet]
        public IEnumerable<SearchedUserDTO> FindByLogin(int userId, string query)
        {
            ICustomRequest custom = (ICustomRequest)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ICustomRequest));

            return custom.Load<IEnumerable<SearchedUserDTO>>(HttpMethod.Get, this.BllPath + "/user/search", new Dictionary<string, string>()
            {
                {  "query", query },
                { "userId", userId.ToString() }
            });
        }


        [Route("api/user/register")]
        [HttpPost]
        public RegisterResponseViewModel Register()
        {
            NameValueCollection parameters = HttpContext.Current.Request.Params;
            UserDTO user = new UserDTO()
            {
                Company = parameters["Company"],
                Email = parameters["Email"],
                Facebook = parameters["Facebook"],
                Login = parameters["Login"],
                Password = parameters["Password"],
                VK = parameters["VK"]
            };

            var files = HttpContext.Current.Request.Files;

            if (files.Count == 0 || files[0] == null)
            {
                user.Image = "/Images/ava.png";
            }
            else
            {
                HttpPostedFile Image = files[0];
                user.Image = this.saveImage(Image);
            }

            var response = loader.PostItem(user);

            return new RegisterResponseViewModel(response, TokenCollection.GenerateToken(user));
        }

        [HttpPut]
        public CUDResponseView UpdateUser(UserDTO user)
        {
            if (user == null) { return null; }

            return loader.PutItem(user);
        }

        [HttpDelete]
        public CUDResponseView DeleteUser(int id)
        {
            if (id <= 0)
            {
                return null;
            }

            return loader.DeleteItem(id);
        }
    }
}
