﻿using Loader;
using Loader.DTO;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Loader.CustomRequests;
using SL.Models;
using System.Linq;

namespace SL.Controllers
{
    public class RequestController : ApiController
    {
        private ILoader<MeetingRequestDTO> loader = DependencyResolver.Current.GetService<ILoader<MeetingRequestDTO>>();

        private readonly string BllPath = "http://localhost:17995/api/";

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/request/received")]
        public IEnumerable<MeetingRequestViewModel> GetReceivedRequests(int userId)
        {
            ICustomRequest custom = DependencyResolver.Current.GetService<ICustomRequest>();

            return custom.Load<IEnumerable<MeetingRequestDTO>>(HttpMethod.Get, this.BllPath + "request/received", new Dictionary<string, string>()
            {
                { "userId", userId.ToString() }
            }).Select(dto => new MeetingRequestViewModel(dto) );
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/request/sended")]
        public IEnumerable<MeetingRequestViewModel> GetSentRequests(int userId)
        {
            ICustomRequest custom = DependencyResolver.Current.GetService<ICustomRequest>();

            return custom.Load<IEnumerable<MeetingRequestDTO>>(HttpMethod.Get, this.BllPath + "request/sended", new Dictionary<string, string>()
            {
                { "userId", userId.ToString() }
            }).Select(dto => new MeetingRequestViewModel(dto));
        }

        [System.Web.Http.HttpPost]
        public CUDResponseView CreateRequest(MeetingRequestDTO req)
        {
            if (req == null) { return CUDResponseView.BuildErrorResponse("Undefined request"); }
            return loader.PostItem(req);
        }

        [System.Web.Http.HttpPut]
        public CUDResponseView UpdateRequest(MeetingRequestDTO req)
        {
            if (req == null) { return CUDResponseView.BuildErrorResponse("Undefined request"); }
            return loader.PutItem(req);
        }

        [System.Web.Http.HttpDelete]
        public CUDResponseView DeleteRequest(int id)
        {
            if (id <= 0) { return CUDResponseView.BuildErrorResponse("Undefined request"); }
            return loader.DeleteItem(id);
        }
    }
}
