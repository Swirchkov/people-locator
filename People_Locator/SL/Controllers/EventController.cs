﻿using Loader;
using Loader.CustomRequests;
using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SL.Controllers
{
    public class EventController : ApiController
    {
        private ILoader<EventDTO> loader = (ILoader<EventDTO>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ILoader<EventDTO>));

        private readonly string BllPath = "http://localhost:17995/api";

        [HttpGet]
        public IEnumerable<EventDTO> GetAll() => loader.LoadAll();

        [HttpGet]
        public EventDTO GetById(int id) => loader.LoadById(id);

        [HttpGet]
        [Route("api/event/eventsByDay")]
        public IEnumerable<EventDTO> GetEventsByDay(int year, int month, int day, int userId, int scheduleUserId)
        {
            ICustomRequest custom = (ICustomRequest)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ICustomRequest));

            return custom.Load<IEnumerable<EventDTO>>(HttpMethod.Get, this.BllPath + "/event/eventsOfDay", new Dictionary<string, string>() {
                { "year", year.ToString() },
                { "month", month.ToString() },
                { "day", day.ToString() },
                { "userId", userId.ToString() },
                { "scheduleUserId", scheduleUserId.ToString() }
            });

        }

        [HttpPost]
        public CUDResponseView CreateEvent(EventDTO ev)
        {
            if (ev == null) { return null; }
            return loader.PostItem(ev);
        }

        [HttpPut]
        public CUDResponseView UpdateEvent(EventDTO ev)
        {
            if (ev == null) { return null; }
            return loader.PutItem(ev);
        }

        [HttpDelete]
        public CUDResponseView DeleteEvent(int id)
        {
            if (id <= 0) { return null; }
            return loader.DeleteItem(id);
        }
    }
}
