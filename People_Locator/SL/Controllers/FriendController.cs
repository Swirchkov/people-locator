﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Loader;
using Loader.DTO;
using Loader.CustomRequests;

namespace SL.Controllers
{
    public class FriendController : ApiController
    {
        private ILoader<FriendDTO> loader = (ILoader<FriendDTO>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ILoader<FriendDTO>));

        private readonly string BllPath = "http://localhost:17995/api";

        [HttpGet]
        [Route("api/friends/getUserFriends")]
        public IEnumerable<SearchedUserDTO> GetUserFriends(int userId)
        {
            ICustomRequest custom = (ICustomRequest)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ICustomRequest));

            return custom.Load<IEnumerable<SearchedUserDTO>>(HttpMethod.Get, this.BllPath + "/friends/getUserFriends", new Dictionary<string, string>()
            {
                { "id", userId.ToString() } 
            });
        }

        [HttpPost]
        public CUDResponseView CreateFriend(FriendDTO friend)
        {
            if (friend == null) { return CUDResponseView.BuildErrorResponse(""); }
            return loader.PostItem(friend);
        }

        [HttpPut]
        public CUDResponseView UpdateFriend(FriendDTO friend)
        {
            if (friend == null) { return CUDResponseView.BuildErrorResponse(null); }
            return loader.PutItem(friend);
        }

        public CUDResponseView DeleteFriend(int user1Id, int user2Id)
        {
            if (user1Id <= 0 || user2Id <= 0) { return CUDResponseView.BuildErrorResponse("Invalid id"); }
            return loader.DeleteItem(user1Id, user2Id);
        }
    }
}
