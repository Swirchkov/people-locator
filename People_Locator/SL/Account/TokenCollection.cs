﻿using Loader;
using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SL.Account
{
    static class TokenCollection
    {
        private static string key = "People locator";

        public static string GenerateToken(UserDTO user)
        {
            return TripleEncryptor.Encrypt(user.Login, key);
        }

        public static UserDTO GetByToken(string token)
        {
            string login = TripleEncryptor.Decrypt(token, key);

            ILoader<UserDTO> loader = DependencyResolver.Current.GetService<ILoader<UserDTO>>();

            return loader.LoadAll().First(user => user.Login == login);
        }

    }
}
