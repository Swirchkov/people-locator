﻿using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SL.Models
{
    public class RegisterUserViewModel
    {
        public UserDTO User { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}
