﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loader.DTO;

namespace SL.Models
{
    public class RegisterResponseViewModel : CUDResponseView
    {
        public string Token { get; set; }

        public RegisterResponseViewModel() { }

        public RegisterResponseViewModel(CUDResponseView response, string token)
        {
            this.ExceptionMessage = response.ExceptionMessage;
            this.ExceptionType = response.ExceptionType;
            this.IsSuccess = response.IsSuccess;
            this.Token = token;
        }
    }
}
