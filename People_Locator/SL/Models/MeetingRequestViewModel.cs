﻿using Loader;
using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SL.Models
{
    public class MeetingRequestViewModel
    {
        public int Id { get; set; }
        public UserDTO Requester { get; set; }
        public UserDTO Receiver { get; set; }
        public string RequestText { get; set; }
        public string AnswerText { get; set; }
        public DateTime Date { get; set; }

        public MeetingRequestViewModel(MeetingRequestDTO dto)
        {
            this.Id = dto.Id;
            this.RequestText = dto.RequestText;
            this.AnswerText = dto.AnswerText;
            this.Date = dto.FinishDate;

            ILoader<UserDTO> loader = DependencyResolver.Current.GetService<ILoader<UserDTO>>();
            this.Receiver = loader.LoadById(dto.ReceiverId);
            this.Requester = loader.LoadById(dto.RequesterId);
        }

    }
}
