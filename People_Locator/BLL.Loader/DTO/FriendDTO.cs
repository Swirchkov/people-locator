﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loader.DTO
{
    public class FriendDTO : BaseDTO
    {
        public int User1Id { get; set; }
        public int User2Id { get; set; }
        public bool IsAccepted { get; set; }

        internal override string pathToActions
        {
            get
            {
                return "friends";
            }
        }
    }
}
