﻿using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loader.DTO
{
    public class MeetingRequestDTO : BaseDTO
    {
        public int Id { get; set; }
        public int RequesterId { get; set; }
        public int ReceiverId { get; set; }
        public string RequestText { get; set; }
        public string AnswerText { get; set; }
        public DateTime FinishDate { get; set; }

        internal override string pathToActions
        {
            get
            {
                return "request";
            }
        }
    }
}
