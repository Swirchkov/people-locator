﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loader.DTO
{
    public class FriendUserDTO
    {
        public FriendUserDTO(bool isAccepted, UserDTO user)
        {
            this.IsAccepted = isAccepted;
            this.User = user;
        }

        public bool IsAccepted { get; set; }
        public UserDTO User { get; set; }
    }
}
