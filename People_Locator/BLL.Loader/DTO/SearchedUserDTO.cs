﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loader.DTO
{
    public class SearchedUserDTO
    {
        public SearchedUserDTO() { }

        public SearchedUserDTO(UserRelation relation, UserDTO user)
        {
            this.Relation = relation;
            this.User = user;
        }

        public UserDTO User { get; set; }
        public UserRelation Relation { get; set; }
    }

    public enum UserRelation
    {
        NoRelation, 
        SentRequest, 
        ReceivedRequest,
        Friend
    }
}
