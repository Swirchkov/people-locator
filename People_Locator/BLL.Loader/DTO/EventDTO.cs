﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loader.DTO
{
    public class EventDTO : BaseDTO
    {
        public int EventId { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Place { get; set; }
        public EventAvailabilityDTO AvailableTo { get; set; }
        public int UserId { get; set; }

        internal override string pathToActions
        {
            get
            {
                return "event";
            }
        }
    }

     public enum EventAvailabilityDTO {
        Private,
        Friends,
        Public
    };
}
