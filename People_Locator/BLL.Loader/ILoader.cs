﻿using Loader.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loader
{
    public interface ILoader<T> where T : class
    {
        IEnumerable<T> LoadAll();
        T LoadById(params int[] ids);
        CUDResponseView PostItem(T item);
        CUDResponseView PutItem(T item);
        CUDResponseView DeleteItem(params int[] ids);
    }
}
