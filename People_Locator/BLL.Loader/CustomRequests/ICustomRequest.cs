﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Loader.CustomRequests
{
    public interface ICustomRequest
    {
        T Load<T>(HttpMethod method, string url, Dictionary<string, string> parameters);
    }
}
