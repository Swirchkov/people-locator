﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Loader.CustomRequests
{
    class CustomRequest : ICustomRequest
    {
        public T Load<T>(HttpMethod method, string url, Dictionary<string, string> parameters)
        {
            HttpRequestMessage req;

            if (!this.shouldUseMethodBody(method))
            {
                url = this.writeParametersToUrl(url, parameters);
                req = new HttpRequestMessage(method, url);
            }
            else
            {
                req = new HttpRequestMessage(method, url);
                req.Content = new StringContent(JsonConvert.SerializeObject(parameters));
                req.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            }

            HttpResponseMessage response;

            using (HttpClient client = new HttpClient())
            {
                response = client.SendAsync(req).Result;
            }

            string content = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(content);
        }

        private string writeParametersToUrl(string url, Dictionary<string, string> parameters)
        {
            if (parameters.Count == 0) { return url; }

            url += "?";

            foreach (string key in parameters.Keys)
            {
                url += key + "=" + Uri.EscapeDataString(parameters[key]) + "&";
            }

            return url.Substring(0, url.Length - 1);
        }

        private bool shouldUseMethodBody(HttpMethod method)
        {
            return !(method == HttpMethod.Get || method == HttpMethod.Head || method == HttpMethod.Delete);
        }
    }
}
